//
//  PayloadToPayoffResolver.m
//  Digimarc Mobile SDK: DMSDemo
//
//  Created by localTstewart on 8/23/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#import "PayloadToPayoffResolver.h"
#import "DMResolver.h"

@interface PayloadToPayoffResolver()

@property NSMutableArray* payloadsPendingResolve;

@end

@implementation PayloadToPayoffResolver

- (id) init {
    self = [super init];
    if(self) {
        self.payloadsPendingResolve = [[NSMutableArray alloc] init];
        self.resolverIsAvailable = NO;
    }
    return self;
}

- (void) dealloc {
    self.payloadsPendingResolve = nil;
}

#pragma mark - DMPayoffsHistoryDelegate notifications

- (void) postError:(NSError*)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(self.delegate && [self.delegate respondsToSelector:@selector(payloadResolver:reportedError:)]) {
            [self.delegate payloadResolver:self reportedError:error];
        }
    });
}

- (void) postWarningFrom:(NSString*)source message:(NSString*)msg {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(self.delegate && [self.delegate respondsToSelector:@selector(payloadResolver:reportedWarningFrom:message:)]) {
            [self.delegate payloadResolver:self reportedWarningFrom:source message:msg];
        }
    });
}

- (void) postResolvedPayoffWithResult:(DMResolveResult*)result {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(self.delegate) {
            if([self.delegate respondsToSelector:@selector(payloadResolver:resolvedNewPayoffWithResult:)]) {
                [self.delegate payloadResolver:self resolvedNewPayoffWithResult:result];
            }
        }
    });
}


#pragma mark - optional resolve payloads to payoffs, recent payoffs history list

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Making a Payload Resolve Request] */
// Requesting payload resolution
//
// This method is the entrypoint for requesting resolution of a payload.
//
- (void) resolvePayload:(DMPayload*)payload {
    NSLog(@"PayloadToPayoffResolver resolvePayload:%@", payload);
    
    // Add the new payload to a pending queue.
	//
    [self.payloadsPendingResolve addObject:payload];
    
    // Check for resolver and network availability
    //
    if(! self.resolverIsAvailable) {
    
    	// If unavailable, check and update the status
    	// 
        [self updateResolverIsAvailable];
    } else  {
    
    	// If available, begin resolving pending payloads
    	//
        [self startPayloadResolveQueries];
    }
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Making a Payload Resolve Request] */

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Update Resolver Available Status] */
// Update resolver status
//
- (void) updateResolverIsAvailable {
    if(self.resolverIsAvailable) {
        [self startPayloadResolveQueries];
    } else {
        
        // Invoke the DMResolver resolverAvailable method
        //
        // This establishes a network connection to the resolver host,
        // invokes the resolver service, and exercises the resolver service web API.
        //
        [self.resolver resolverAvailable:^(BOOL available, NSError *error) {
            if(error && error.code) {
                [self postError:error];
                self.resolverIsAvailable = NO;
            }
            if(!available) {

                // On failure, wait and try again
		        //
                [self performSelector:@selector(updateResolverIsAvailable) withObject:nil afterDelay:10];
            } else {
            
                // On success, begin resolving payloads
		        //
                self.resolverIsAvailable = YES;
                [self startPayloadResolveQueries];
            }
        }];
    }
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Update Resolver Available Status] */

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Perform Payload Resolve Queries] */
// Perform queries for payload resolution
//
- (void) startPayloadResolveQueries {
    while(self.resolverIsAvailable && self.payloadsPendingResolve.count > 0) {
        
        DMPayload* pendingPayload = [self.payloadsPendingResolve objectAtIndex:0];
        [self.payloadsPendingResolve removeObjectAtIndex:0];

        // DMResolver internally uses NSURLConnection, which in turn uses objectiveC message-passing
        // to communicate between control and I/O threads.  This works only if the initial DMResolver
        // entrypoint call is invoked on a thread that has an active RunLoop.  Switching over to the
        // main thread is an easy way to satisfy this requirement.  All actual I/O will be performed
        // on background threads internally.
        //
        dispatch_async(dispatch_get_main_queue(), ^{

            // Now running on main thread...
            // NSLog(@"dispatched from main queue: %@", pendingPayload);
            
            if(! self.resolverIsAvailable) {
                
                // resolver has gone down between time of queueing and time of executing this payload,
                // just put the payload back into the pending list to re-execute once resolver recovers
                //
                [self.payloadsPendingResolve addObject:pendingPayload];
                
            } else {
                
                // Invoke the DMResolver resolve:completionBlock: async web query API
                //
                // If successful, the resolver  translates the supplied payload into the associated payoff --
                // a dictionary of metadata as defined by the content owner for this payload 
                // in the Digimarc Online Service Portal.
                //
                // The resulting dictionary is returned in the HTTP response and is parsed by DMResolver
                // into explicit fields in the DMResolveResult, which wraps a copy of the original
                // DMPayload object, a new DMStandardPayoff object, a type code for the new payoff,
                // and miscellaneous diagnostic info about the overall resolve query round trip times.
                //
                
                // Define the completion block handler (separated out here to keep Doxygen happy)
                //
                ResolveCompletionBlock resolveResultHandler = ^(DMResolveResult *result, NSError *error) {
                    
                    // Now running in async notification completion block...
                    // NSLog(@"in completion block from resolve of payload: %@", pendingPayload);
                    
                    if(error && error.code) {
                        // resolve failed, post an error, and queue reverify resolver service is still available
                        [self postError:error];
                        self.resolverIsAvailable = NO;
                        [self updateResolverIsAvailable];
                    } else {
                        // resolve completed with payoff in new DMResolveResult, notify the delegate
						[self postResolvedPayoffWithResult:result];
                    }
                };
                
                // Invoke the resolver itself, which runs an asynchronous query and when done 
                // calls the completion block defined above
                //
                // NSLog(@"invoking DMResolver for payload: %@", pendingPayload);
				
                [self.resolver resolve:pendingPayload completionBlock:resolveResultHandler];
            }
        });
    }
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Perform Payload Resolve Queries] */

@end
