//
//  PayoffItem.m
//  DMSDemo
//
//  Created by Sinclair, Eoin on 2/13/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import "PayoffItem.h"

@implementation PayoffItem

-(id)init {
    return [self initWithImageData:nil date:[NSDate date] resolveResult:nil title:nil subtitle:nil actionToken:nil content:nil];
}

-(id)initWithImageData:(NSData*)data date:(NSDate*)date resolveResult:(DMPayload*)payload title:(NSString *)title subtitle:(NSString *)subtitle actionToken:(NSString *)token content:(NSString *)content {
    if (self = [super init]) {
        self.imageData      = data;
        self.date           = date;
        self.payload        = payload;
        self.title          = title;
        self.subtitle       = subtitle;
        self.actionToken    = token;
        self.content        = content;
        if (!self.imageData && !self.date && !self.payload && !self.title) {
            NSLog(@"Empty history item allocated; returning nil.");
            return nil;
        }
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    NSData *tempImageData       = [aDecoder decodeObjectForKey:kImageDataKey];
    NSDate *tempDate            = [aDecoder decodeObjectForKey:kDateKey];
    DMPayload *tempPayload      = [aDecoder decodeObjectForKey:kPayloadKey];
    NSString *tempTitle         = [aDecoder decodeObjectForKey:kTitleKey];
    NSString *tempSubtitle      = [aDecoder decodeObjectForKey:kSubtitleKey];
    NSString *tempToken         = [aDecoder decodeObjectForKey:kTokenKey];
    NSString *tempContent       = [aDecoder decodeObjectForKey:kContentKey];
    
    return [self initWithImageData:tempImageData date:tempDate resolveResult:tempPayload title:tempTitle subtitle:tempSubtitle actionToken:tempToken content:tempContent];
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.imageData     forKey:kImageDataKey];
    [aCoder encodeObject:self.date          forKey:kDateKey];
    [aCoder encodeObject:self.payload       forKey:kPayloadKey];
    [aCoder encodeObject:self.title         forKey:kTitleKey];
    [aCoder encodeObject:self.subtitle      forKey:kSubtitleKey];
    [aCoder encodeObject:self.actionToken   forKey:kTokenKey];
    [aCoder encodeObject:self.content       forKey:kContentKey];
}

@end
