//
//  PayoffDatabase.h
//  DMSDemo
//
//  Created by Sinclair, Eoin on 3/1/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PayoffDatabase : NSObject

+(NSMutableArray *)loadPayoffDocs;
+(NSString *)nextPayoffDocPath;

@end
