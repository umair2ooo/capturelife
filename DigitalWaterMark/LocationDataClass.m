#import "LocationDataClass.h"

@implementation LocationDataClass

@synthesize location_lat;
@synthesize location_long;
@synthesize location_storeName;
@synthesize location_storeImageURL;
@synthesize location_nearestStore;
@synthesize location_Asle;
@synthesize location_Bay;

-(void)setLocation_storeName:(NSString *)location_storeNamex
{
    if (location_storeNamex)
    {
        location_storeName = location_storeNamex;
    }
    else
    {
        location_storeName = @"";
    }
}


-(void)setLocation_storeImageURL:(NSURL *)location_storeImageURLx
{
    if (location_storeImageURLx)
    {
        location_storeImageURL = location_storeImageURLx;
    }
    else
    {
        location_storeImageURL = nil;
    }
}


-(void)setLocation_lat:(float)location_latx
{
    if (location_latx)
    {
        location_lat = location_latx;
    }
    else
    {
        location_lat = 0.0;
    }
}


-(void)setLocation_long:(float)location_longx
{
    if (location_longx)
    {
        location_long = location_longx;
    }
    else
    {
        location_long = 0.0;
    }
}



-(void)setLocation_nearestStore:(BOOL)location_nearestStorex
{
    if (location_nearestStorex)
    {
        location_nearestStore = location_nearestStorex;
    }
    else
    {
        location_nearestStore = false;
    }
}


-(void)setP_Asle:(NSString *)p_Aslex
{
    if (p_Aslex)
    {
        location_Asle = p_Aslex;
    }
    else
    {
        location_Asle = @"-";
    }
}


-(void)setP_Bay:(NSString *)p_Bayx
{
    if (p_Bayx)
    {
        location_Bay = p_Bayx;
    }
    else
    {
        location_Bay = @"-";
    }
}

@end