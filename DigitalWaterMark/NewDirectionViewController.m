#import "NewDirectionViewController.h"

#import "LocationDataClass.h"
#import "MapView.h"
#import "Place.h"

#define k_gap 5.0

#define k_lat "lat"
#define k_long "long"
#define k_storeName "storeName"

#define k_float(x) [NSNumber numberWithFloat:x]
#define D(...) [NSDictionary dictionaryWithObjectsAndKeys:__VA_ARGS__, nil]


@interface NewDirectionViewController ()<UIPickerViewDataSource, UIPickerViewDelegate, CLLocationManagerDelegate, MKMapViewDelegate>
{
    CLLocationDistance minimumDistance;
}
@property(nonatomic, weak)LocationDataClass *location_inMap;

@property(nonatomic, strong)UIPickerView *pickerView;
@property(nonatomic, strong)MapView* mapView;
@property (nonatomic, strong)CLLocation *location_current;
@property (nonatomic, strong)NSDictionary *dic_nearestStore;

@property (nonatomic, strong) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UITextField *textField_selectStore;

@end

@implementation NewDirectionViewController

@synthesize pickerView = _pickerView;
@synthesize mapView = _mapView;
@synthesize location_current = _location_current;
@synthesize locationManager = _locationManager;
@synthesize dic_nearestStore = _dic_nearestStore;
@synthesize array_stores = _array_stores;
@synthesize location_inMap = _location_inMap;

#pragma mark - creating map and drawing path
-(void)method_createMapAndDrawPath
{
    self.mapView = [[MapView alloc] initWithFrame:
                        CGRectMake(k_gap,
                                   self.textField_selectStore.frame.origin.y+self.textField_selectStore.frame.size.height+k_gap,
                                   self.view.frame.size.width-(k_gap*2),
                                   self.view.frame.size.height-(self.textField_selectStore.frame.origin.y+self.textField_selectStore.frame.size.height+k_gap)-k_gap)];
    
    [self.view addSubview:self.mapView];
    
    self.textField_selectStore.text = self.location_inMap.location_storeName;
    
    [self method_dropPins_destination:self.location_inMap];
}

-(void)method_dropPins_destination:(LocationDataClass *)locationDC
{
    Place* home = [[Place alloc] init];
    home.name = locationDC.location_storeName;
    //    home.description = @"Sweet home";
    home.latitude = locationDC.location_lat;
    home.longitude = locationDC.location_long;
    
    
    Place* office = [[Place alloc] init];
    office.name = @"Current Location";
    //    office.description = @"Bad office";
    office.latitude = self.location_current.coordinate.latitude;
    office.longitude = self.location_current.coordinate.longitude;
    
    
    [self.mapView showRouteFrom:home to:office];
    
    home = nil;
    office = nil;
}

#pragma mark - current location
-(void)method_currentLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        //for background and foreground
        //[self.locationManager requestAlwaysAuthorization]
        //foreground
        [self.locationManager requestWhenInUseAuthorization];
    }

    
    
    
    [self.locationManager startUpdatingLocation];
}


#pragma mark - find nearest store
-(void)method_findNearestStore_destinationDic:(LocationDataClass *)locaitonDC
{
    CLLocation *location_destination = [[CLLocation alloc] initWithLatitude:locaitonDC.location_lat
                                                                  longitude:locaitonDC.location_long];
    
    
    CLLocationDistance distance = [self.location_current distanceFromLocation:location_destination];
    
    if (minimumDistance)
    {
        if (distance < minimumDistance)
        {
            minimumDistance = distance;
            self.dic_nearestStore = nil;
            self.location_inMap = locaitonDC;
        }
    }
    else
    {
        minimumDistance = distance;
        self.location_inMap = locaitonDC;
    }
    
    location_destination = nil;
    
    NSLog(@"store distance: %f", distance/1000);                // in KM
}



#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"NearBy Stores";
    
    [self.array_stores enumerateObjectsUsingBlock:^(LocationDataClass *obj, NSUInteger idx, BOOL *stop)
     {
         DLog(@"%@", obj.location_storeName);
         DLog(@"%f", obj.location_lat);
         DLog(@"%f", obj.location_long);
     }];
    
    [self method_addPickerInKeyBoard];
    
    [self.pickerView selectRow:0 inComponent:0 animated:NO];
    
    [self method_currentLocation];
    
    self.mapView.userInteractionEnabled = NO;
    
//    [self performSelector:@selector(method_createMapAndDrawPath) withObject:nil afterDelay:0.1];
}


-(void)viewWillDisappear:(BOOL)animated
{
    self.pickerView.delegate = nil;
    self.pickerView = nil;
    self.mapView = nil;
    
    self.array_stores = nil;

    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = nil;
    self.locationManager = nil;
    self.location_current = nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - add picker in text keyBoard
-(void)method_addPickerInKeyBoard
{
    self.pickerView = [[UIPickerView alloc] init];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    // ... ...
    self.textField_selectStore.inputView = self.pickerView;
    
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                        target:nil
                                                                        action:nil],
                           
                           [[UIBarButtonItem alloc]initWithTitle:@"Done"
                                                           style:UIBarButtonItemStyleDone
                                                          target:self
                                                          action:@selector(action_done)], nil];
    
    [numberToolbar sizeToFit];
    self.textField_selectStore.inputAccessoryView = numberToolbar;
    
    numberToolbar = nil;
}


#pragma mark - bar button
-(void)action_done
{
    [self.textField_selectStore resignFirstResponder];
    
    self.location_inMap = [self.array_stores objectAtIndex:[self.pickerView selectedRowInComponent:0]];
    
    if (self.location_current && ![self.textField_selectStore.text isEqualToString:self.location_inMap.location_storeName])
    {
        self.textField_selectStore.text = self.location_inMap.location_storeName;
        
        [self method_dropPins_destination:self.location_inMap];
    }
}



#pragma mark - picker delegate and data source
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.array_stores count]?[self.array_stores count]:0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    self.location_inMap = [self.array_stores objectAtIndex:row];
    
    return self.location_inMap.location_storeName;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}



#pragma mark - didUpdateToLocation
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if (!self.location_current)
    {
        NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
        NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        
        
        self.location_current = newLocation;
        
        
        for (unsigned i =0; i<[self.array_stores count]; i++)
        {
            [self method_findNearestStore_destinationDic:[self.array_stores objectAtIndex:i]];
        }
        
        [self method_createMapAndDrawPath];
    }
}

@end