#import "ProductsDataClass.h"

#import "LocationDataClass.h"

#define k_lat "lat"
#define k_long "long"
#define k_storeName "storeName"
#define k_storeMap "storeMap"
#define k_nearestStore "nearestStore"
#define k_aisle "aisle"
#define k_bay "bay"


@implementation ProductsDataClass

@synthesize p_id;
@synthesize p_name;
@synthesize p_rating;
@synthesize p_price;
@synthesize p_sale;
@synthesize p_inStock;
@synthesize p_model;
@synthesize p_skuNumber;
@synthesize p_imageURL;
@synthesize p_imageURL_AR;
@synthesize array_location;
@synthesize p_description;



-(void)setP_id:(NSString *)p_idx
{
    if (p_idx)
    {
        p_id = p_idx;
    }
    else
    {
        p_id = @"";
    }
}


-(void)setP_name:(NSString *)p_namex
{
    if (p_namex)
    {
        p_name = p_namex;
    }
    else
    {
        p_name = @"";
    }
}


-(void)setP_rating:(int)p_ratingx
{
    if (p_ratingx)
    {
        p_rating = p_ratingx;
    }
    else
    {
        p_rating = 0;
    }
}


-(void)setP_price:(NSString *)p_pricex
{
    if (p_pricex)
    {
        p_price = [NSString stringWithFormat:@"$: %@", p_pricex];
    }
    else
    {
        p_price = @"$0";
    }
}


-(void)setP_sale:(NSString *)p_salex
{
    if (p_salex)
    {
        p_sale = p_salex;
    }
    else
    {
        p_sale = @"0";
    }
}


-(void)setP_inStock:(NSString *)p_inStockx
{
    if (p_inStockx)
    {
        p_inStock = p_inStockx;
    }
    else
    {
        p_inStock = @"0";
    }
}


-(void)setP_model:(NSString *)p_modelx
{
    if (p_modelx)
    {
        p_model = p_modelx;
    }
    else
    {
        p_model = @"-";
    }
}


-(void)setP_skuNumber:(NSString *)p_skuNumberx
{
    if (p_skuNumberx)
    {
        p_skuNumber = p_skuNumberx;
    }
    else
    {
        p_skuNumber = @"-";
    }
}



-(void)setP_description:(NSString *)p_descriptionx
{
    if (p_descriptionx && ![p_descriptionx isKindOfClass:[NSNull class]])
    {
        p_description = p_descriptionx;
    }
    else
    {
        p_description = @"-";
    }
}


-(void)setP_imageURL:(NSURL *)p_imageURLx
{
    if (p_imageURLx)
    {
        p_imageURL = p_imageURLx;
    }
    else
    {
        p_imageURL = nil;
    }
}


-(void)setP_imageURL_AR:(NSURL *)p_imageURL_ARx
{
    if (p_imageURL_ARx)
    {
        p_imageURL_AR = p_imageURL_ARx;
    }
    else
    {
        p_imageURL_AR = nil;
    }
}


-(void)setArray_location:(NSMutableArray *)array_locationx
{
    if (array_locationx)
    {
        array_location = [[NSMutableArray alloc] init];
        [array_locationx enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
        {
            LocationDataClass *obj_ = [[LocationDataClass alloc] init];
            obj_.location_storeName = [obj valueForKey:@k_storeName];
            obj_.location_storeImageURL = [NSURL URLWithString:[obj valueForKey:@k_storeMap]];
            obj_.location_lat = [[obj valueForKey:@k_lat] floatValue];
            obj_.location_long = [[obj valueForKey:@k_long] floatValue];
            obj_.location_nearestStore = [[NSNumber numberWithInt:[[obj valueForKey:@k_nearestStore] intValue]] boolValue];
            obj_.location_Asle = [obj valueForKey:@k_aisle];
            obj_.location_Bay = [obj valueForKey:@k_bay];

            [array_location addObject:obj_];
            obj_ = nil;
        }];
    }
}

@end