//
//  main.m
//  DigitalWaterMark
//
//  Created by RC Mad Team on 08/10/2014.
//  Copyright (c) 2014 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}