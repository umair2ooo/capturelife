
#import <Foundation/Foundation.h>

@interface Singleton : NSObject
{
}

+(Singleton *)retriveSingleton;


-(BOOL) method_NSStringIsValidEmail:(NSString *)checkString;
-(BOOL)method_phoneNumberValidation:(NSString *)phoneNumber;
-(BOOL) method_checkOnlySpaces:(NSString *)str;
-(void) method_networkError;



@end