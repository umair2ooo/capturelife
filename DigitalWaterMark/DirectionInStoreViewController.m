#import "DirectionInStoreViewController.h"

#import "LocationDataClass.h"
#import "AsyncImageView.h"

@interface DirectionInStoreViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>
{
}

@property(nonatomic, weak)LocationDataClass *location_inStore;
@property(nonatomic, strong)UIPickerView *pickerView;

@property (weak, nonatomic) IBOutlet UITextField *textField_storeName;
@property (weak, nonatomic) IBOutlet AsyncImageView *imageView_insideStore;
@property (weak, nonatomic) IBOutlet UILabel *label_aisle;
@property (weak, nonatomic) IBOutlet UILabel *label_bay;

@end

@implementation DirectionInStoreViewController

@synthesize location_inStore;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Aisle & Bay";
    
    [self method_addPickerInKeyBoard];
    
    [self.pickerView selectRow:0 inComponent:0 animated:NO];
    
//    DLog(@"self.array_stores: %@", self.array_stores);
//    
//    [self.array_stores enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
//     {
//         self.location_inStore = (LocationDataClass *)obj;
//
//         DLog(@"%@", self.location_inStore.location_storeImageURL);
//         DLog(@"%@", self.location_inStore.location_storeName);
//
//     }];
    
    
    self.location_inStore = [_array_stores objectAtIndex:0];
    
    self.textField_storeName.text = self.location_inStore.location_storeName;
    self.label_aisle.text = [NSString stringWithFormat:@"Aisle: %@", self.location_inStore.location_Asle];
    self.label_bay.text = [NSString stringWithFormat:@"Bay: %@", self.location_inStore.location_Bay];

//    //cancel loading previous image for cell
//    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:self.imageView_insideStore];


    //set placeholder image or cell won't update when image is loaded
    self.imageView_insideStore.image = [UIImage imageNamed:@"Placeholder.png"];
    
    //load the image
    [self.imageView_insideStore loadImageFromURL:[self.location_inStore.location_storeImageURL absoluteString]];
//    self.imageView_insideStore.imageURL = self.location_inStore.location_storeImageURL;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(void)viewWillDisappear:(BOOL)animated
{
    self.pickerView.delegate = nil;
    self.pickerView = nil;
    
    self.array_stores = nil;
}



#pragma mark - add picker in text keyBoard
-(void)method_addPickerInKeyBoard
{
    self.pickerView = [[UIPickerView alloc] init];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    // ... ...
    self.textField_storeName.inputView = self.pickerView;
    
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                        target:nil
                                                                        action:nil],
                           
                           [[UIBarButtonItem alloc]initWithTitle:@"Done"
                                                           style:UIBarButtonItemStyleDone
                                                          target:self
                                                          action:@selector(action_done)], nil];
    
    [numberToolbar sizeToFit];
    self.textField_storeName.inputAccessoryView = numberToolbar;
    
    numberToolbar = nil;
}


#pragma mark - bar button
-(void)action_done
{
    [self.textField_storeName resignFirstResponder];
    
    
    self.location_inStore = [_array_stores objectAtIndex:[self.pickerView selectedRowInComponent:0]];

    self.textField_storeName.text = self.location_inStore.location_storeName;
    self.label_aisle.text = [NSString stringWithFormat:@"Aisle: %@", self.location_inStore.location_Asle];
    self.label_bay.text = [NSString stringWithFormat:@"Bay: %@", self.location_inStore.location_Bay];
    
    //cancel loading previous image for cell
//    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:self.imageView_insideStore];
    
    
    //set placeholder image or cell won't update when image is loaded
    self.imageView_insideStore.image = [UIImage imageNamed:@"Placeholder.png"];
    
    //load the image
    [self.imageView_insideStore loadImageFromURL:[self.location_inStore.location_storeImageURL absoluteString]];
//    self.imageView_insideStore.imageURL = self.location_inStore.location_storeImageURL;
}



#pragma mark - picker delegate and data source
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.array_stores count]?[self.array_stores count]:0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    self.location_inStore = [self.array_stores objectAtIndex:row];
    return self.location_inStore.location_storeName;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end