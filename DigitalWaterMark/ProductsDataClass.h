#import <Foundation/Foundation.h>

@interface ProductsDataClass : NSObject

@property (nonatomic, strong)NSString *p_id;
@property (nonatomic, strong)NSString *p_name;
@property (nonatomic) int p_rating;
@property (nonatomic, strong)NSString *p_price;
@property (nonatomic, strong)NSString *p_sale;
@property (nonatomic, strong)NSString *p_inStock;
@property (nonatomic, strong)NSString *p_model;
@property (nonatomic, strong)NSString *p_skuNumber;
@property (nonatomic, strong)NSString *p_description;
@property (nonatomic, strong)NSURL *p_imageURL;
@property (nonatomic, strong)NSURL *p_imageURL_AR;

@property(nonatomic, strong)NSMutableArray *array_location;

@end