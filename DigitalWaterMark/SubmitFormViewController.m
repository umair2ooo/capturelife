#import "SubmitFormViewController.h"

#import "Singleton.h"
#import "CDatePickerViewEx.h"
#import "MBProgressHUD.h"
#import "CustomSpinnerView.h"

#define SCROLLVIEW_HEIGHT 460
#define SCROLLVIEW_WIDTH  320

#define SCROLLVIEW_CONTENT_HEIGHT 720
#define SCROLLVIEW_CONTENT_WIDTH  320


@interface SubmitFormViewController ()<UITextFieldDelegate, UIAlertViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, SelectedDate>
{
    BOOL           keyboardVisible;
    CGPoint        offset;
    
    Singleton *single;
    
    NSArray *array_cardType;
}

@property(nonatomic, strong)UIPickerView *pickerView;



@property (weak, nonatomic) IBOutlet UITextField *textField_name;
@property (weak, nonatomic) IBOutlet UITextField *textField_contact;
@property (weak, nonatomic) IBOutlet UITextField *textField_cardType;
@property (weak, nonatomic) IBOutlet UITextField *textField_cardNumber;
@property (weak, nonatomic) IBOutlet UITextField *textField_expiryDate;
@property (weak, nonatomic) IBOutlet UITextField *textField_CVVTwo;
@property (weak, nonatomic) IBOutlet UITextField *textField_email;
@property (weak, nonatomic) IBOutlet UITextField *textField_address;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_CardType;
@property (weak, nonatomic) IBOutlet UITextField *textField_addressTwo;
@property (weak, nonatomic) IBOutlet UITextField *textField_city;
@property (weak, nonatomic) IBOutlet UITextField *textField_state;
@property (weak, nonatomic) IBOutlet UITextField *textField_zipCode;

@property (retain)  MBProgressHUD *progressHUD;
@property (retain)  CustomSpinnerView *customSpinner;


- (IBAction)action_submit:(id)sender;

@end

@implementation SubmitFormViewController

@synthesize productsDataClass_obj = _productsDataClass_obj;


#pragma mark - show and hide loader
-(void)showSpinner
{
    self.progressHUD = [[MBProgressHUD alloc] initWithWindow:self.view.window];
    self.progressHUD.labelText = @"Loading Image";
    self.progressHUD.center = self.view.center;
    self.customSpinner = [[CustomSpinnerView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self.progressHUD setCustomView:self.customSpinner];
    [self.progressHUD setMode:MBProgressHUDModeCustomView];
    [self.view.window addSubview:self.progressHUD];
    [self.progressHUD show:YES];
    
    //    NSTimer *t = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(removeSpinner) userInfo:0 repeats:NO];
    //    [[NSRunLoop mainRunLoop] addTimer:t forMode:NSRunLoopCommo                                                                                                                                                                                                                                                 nModes];
}

-(void)removeSpinner
{
    self.progressHUD.removeFromSuperViewOnHide = YES;
    [self.progressHUD hide:YES];
    [self.customSpinner stopAnimation];
    self.progressHUD = nil;
    self.customSpinner = nil;
}



#pragma mark - add picker in text keyBoard
-(void)method_addPickerInKeyBoard
{
    array_cardType = [NSArray arrayWithObjects:@"Master Card",@"Visa", @"American Express", nil];
    
    
    self.pickerView = [[UIPickerView alloc] init];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    // ... ...
    self.textField_cardType.inputView = self.pickerView;
    
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                        target:nil
                                                                        action:nil],
                           
                           [[UIBarButtonItem alloc]initWithTitle:@"Next"
                                                           style:UIBarButtonItemStyleDone
                                                          target:self
                                                          action:@selector(action_done)], nil];
    
    [numberToolbar sizeToFit];
    self.textField_cardType.inputAccessoryView = numberToolbar;
    
    numberToolbar = nil;
    
    
    
    
    CDatePickerViewEx *datePicker = [[CDatePickerViewEx alloc]init];
    datePicker.delegate_mine = self;
//    [datePicker setDate:[NSDate date]];
//    [datePicker setDatePickerMode:UIDatePickerModeDate];
//    [datePicker addTarget:self action:@selector(updateExpiryDateTextField:) forControlEvents:UIControlEventValueChanged];
    [self.textField_expiryDate setInputView:datePicker];
}

#pragma mark - updateExpiryDateTextField
-(void)updateExpiryDateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)self.textField_expiryDate.inputView;
//    self.textField_expiryDate.text = [NSString stringWithFormat:@"%@",picker.date];
    
    
    NSDate * selected = [picker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM/yy"];
    self.textField_expiryDate.text = [dateFormatter stringFromDate:selected];
}




#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Order Online";

    [self.scroller setContentSize:CGSizeMake(0, 800)];

    single = [Singleton retriveSingleton];
    
    
    [self method_addPickerInKeyBoard];
    [self.pickerView selectRow:0 inComponent:0 animated:NO];
    
    
    DLog(@"productsDataClass_obj.p_id: %@", self.productsDataClass_obj.p_id);
    DLog(@"productsDataClass_obj.p_name: %@", self.productsDataClass_obj.p_name);
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector (keyboardDidShow:)
//                                                 name: UIKeyboardDidShowNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector (keyboardDidHide:)
//                                                 name: UIKeyboardDidHideNotification object:nil];
    
    
    self.scroller.frame = self.view.frame;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.textField_address.delegate = nil;
    self.textField_contact.delegate = nil;
    self.textField_email.delegate = nil;
    self.textField_name.delegate = nil;
    self.scroller.delegate = nil;
    
    self.pickerView.delegate = nil;
    self.pickerView = nil;
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


//-(void) keyboardDidShow: (NSNotification *)notif
//{
//    // If keyboard is visible, return
//    if (keyboardVisible)
//    {
//        NSLog(@"Keyboard is already visible. Ignoring notification.");
//        return;
//    }
//
//    // Get the size of the keyboard.
//    NSDictionary* info = [notif userInfo];
//    NSValue* aValue = [info objectForKey:UIKeyboardBoundsUserInfoKey];
////    NSValue* aValue = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
//    CGSize keyboardSize = [aValue CGRectValue].size;
//    
//    // Save the current location so we can restore
//    // when keyboard is dismissed
//    offset = self.scroller.contentOffset;
//    
//    // Resize the scroll view to make room for the keyboard
//    CGRect viewFrame = self.scroller.frame;
//    viewFrame.size.height -= keyboardSize.height;
//    self.scroller.frame = viewFrame;
//    
//    // Keyboard is now visible
//    keyboardVisible = YES;
//}
//
//
//
//-(void) keyboardDidHide: (NSNotification *)notif
//{
//    // Is the keyboard already shown
//    if (!keyboardVisible)
//    {
//        NSLog(@"Keyboard is already hidden. Ignoring notification.");
//        return;
//    }
//    
//    // Reset the height of the scroll view to its original value
//    self.scroller.frame = CGRectMake(0, 0, SCROLLVIEW_WIDTH, self.view.frame.size.height);
//    
//    // Reset the scrollview to previous location
//    self.scroller.contentOffset = offset;
//    
//    // Keyboard is no longer visible
//    keyboardVisible = NO;	
//}

#pragma mark - text field delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.textField_name)
    {
        [self.textField_contact becomeFirstResponder];
        return NO;
    }
    if (textField == self.textField_contact)
    {
        [self.textField_email becomeFirstResponder];
        return NO;
    }
    if (textField == self.textField_email)
    {
        [self.textField_address becomeFirstResponder];
        return NO;
    }
    if (textField == self.textField_address)
    {
        [self.textField_addressTwo becomeFirstResponder];
        return NO;
    }
    
    if (textField == self.textField_addressTwo)
    {
        [self.textField_city becomeFirstResponder];
        return NO;
    }
    
    if (textField == self.textField_city)
    {
        [self.textField_state becomeFirstResponder];
        return NO;
    }
    
    if (textField == self.textField_state)
    {
        [self.textField_zipCode becomeFirstResponder];
        return NO;
    }
    
    
    [self.textField_zipCode resignFirstResponder];
    return YES;
}

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    
//    if (textField == self.textField_expiryDate)
//    {
//        if ([self.textField_cardType.text length])
//        {
//            return YES;
//        }
//        else
//        {
//            [[[UIAlertView alloc] initWithTitle:nil message:@"Select card type first"
//                                       delegate:nil
//                              cancelButtonTitle:@"Ok"
//                              otherButtonTitles:nil, nil]
//             show];
//            return NO;
//        }
//    }
//    return YES;
//}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.textField_cardType ||
        textField == self.textField_cardNumber ||
        textField == self.textField_CVVTwo ||
        textField == self.textField_expiryDate)
    {
        if ([self.textField_cardType.text length])
        {
            if (textField == self.textField_cardNumber)
            {
                if ([self.textField_cardType.text isEqualToString:@"Master Card"]||[self.textField_cardType.text isEqualToString:@"Visa"])
                {
                    __block NSString *text = [textField text];
                    
                    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
                    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
                    if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
                        return NO;
                    }
                    
                    text = [text stringByReplacingCharactersInRange:range withString:string];
                    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    NSString *newString = @"";
                    while (text.length > 0) {
                        NSString *subString = [text substringToIndex:MIN(text.length, 4)];
                        newString = [newString stringByAppendingString:subString];
                        if (subString.length == 4) {
                            newString = [newString stringByAppendingString:@" "];
                        }
                        text = [text substringFromIndex:MIN(text.length, 4)];
                    }
                    
                    newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
                    
                    if (newString.length >= 20) {
                        return NO;
                    }
                    
                    [textField setText:newString];
                    
                    return NO;
                }
                
                if([self.textField_cardType.text isEqualToString:@"American Express"])
                {
                    if (range.location<=16) {
                        
                        if (range.location==4)
                        {
                            NSMutableString *Text = nil;
                            Text = [NSMutableString stringWithFormat:@"%@",textField.text];
                            [Text insertString:@" " atIndex:4];
                            [textField setText:Text];
                        }
                        
                        if (range.location==11)
                        {
                            NSMutableString *Text = nil;
                            Text = [NSMutableString stringWithFormat:@"%@",textField.text];
                            [Text insertString:@" " atIndex:11];
                            [textField setText:Text];
                        }
                        return YES;
                    }
                    return NO;
                }
                return NO;
            }
            
            if (self.textField_CVVTwo == textField)
            {
                if ([self.textField_cardType.text isEqualToString:@"Master Card"]||[self.textField_cardType.text isEqualToString:@"Visa"])
                {
                    if ([textField.text length] > 2 && ![string isEqualToString:@""])
                    {
                        return NO;
                    }
                }
                else
                {
                    if ([textField.text length] > 3 && ![string isEqualToString:@""])
                    {
                        return NO;
                    }
                }
                return YES;
                
            }
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:nil message:@"Select card type first"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
            return NO;
        }
    }
    
    
    return YES;
}




#pragma mark - method_submitForm
- (void)method_submitForm
{
    DLog(@"%@", [NSString stringWithFormat:@"http://webservice.royalcyber.net/CaptureLife/CaptureLifeWebservice/?method=feedback&format=json&name=%@&email=%@&address=%@&contact=%@&productId=%@&productName=%@",
                 self.textField_name.text,
                 self.textField_email.text,
                 self.textField_address.text,
                 self.textField_contact.text,
                 self.productsDataClass_obj.p_id,
                 self.productsDataClass_obj.p_name]);

//    http://webservice.royalcyber.net/CaptureLife/CaptureLifeWebservice/?method=
//    NSString *str_temp =[NSString stringWithFormat:@"http://119.63.131.236:88/CaptureLife/CaptureLifeFeedback/?method=feedback&format=json&name=%@&email=%@&address=%@&contact=%@&productId=%@&productName=%@",
    
    NSString *str_temp =[NSString stringWithFormat:@"http://webservice.royalcyber.net/CaptureLife/CaptureLifeFeedback//?method=feedback&format=json&name=%@&email=%@&address=%@&contact=%@&productId=%@&productName=%@",
                         self.textField_name.text,
                         self.textField_email.text,
                         self.textField_address.text,
                         self.textField_contact.text,
                         self.productsDataClass_obj.p_id,
                         self.productsDataClass_obj.p_name];
    
    str_temp = [str_temp stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString: str_temp];
    
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://119.63.131.236:88/CaptureLife/CaptureLifeFeedback/?method=feedback&format=json&name=%@&email=%@&address=%@&contact=%@&productId=%@&productName=%@",
//                                       self.textField_name.text,
//                                       self.textField_email.text,
//                                       self.textField_address.text,
//                                       self.textField_contact.text,
//                                       self.productsDataClass_obj.p_id,
//                                       self.productsDataClass_obj.p_name]];
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         [self removeSpinner];
         
         
         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:0
                                                                            error:NULL];
             
             DLog(@"responseData: %@", responseData);
             
             if ([responseData isKindOfClass:[NSNull class]] || responseData == nil)
             {
                 [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                             message:@"There is something is wrong from server side, please try later"
                                            delegate:nil
                                   cancelButtonTitle:@"Ok"
                                   otherButtonTitles:nil, nil]
                  show];
             }
             else
             {
                 [[[UIAlertView alloc] initWithTitle:@"Status"
                                             message:@"Order submitted successfully"//[[responseData valueForKey:@"data"] valueForKey:@"orderStatus"]
                                            delegate:self
                                   cancelButtonTitle:@"Ok"
                                   otherButtonTitles:nil, nil]
                  show];
                 
                 
//                 [16/01/2015 5:55:56 pm] Talha Haroon: {"code":0,"status":404,"data":{"orderStatus":"Successfully Completed","orderStatusCode":1}}
//                 [16/01/2015 5:56:09 pm] Talha Haroon: orderstatusCode: 0 hojaega unsuccessful hua to
             }
         }
         else
         {
             [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                         message:@"There is something is wrong from server side, please try later"
                                        delegate:nil
                               cancelButtonTitle:@"Ok"
                               otherButtonTitles:nil, nil]
              show];
         }
     }];
}




#pragma mark - action_submit
- (IBAction)action_submit:(id)sender
{
//    if ([single method_checkOnlySpaces:self.textField_name.text])
//    {
//        [[[UIAlertView alloc] initWithTitle:@"Sorry"
//                                    message:@"Please enter your name"
//                                   delegate:nil
//                          cancelButtonTitle:@"Ok"
//                          otherButtonTitles:nil, nil]
//         show];
//    }
////    else if ([self.textField_contact.text length] && [single method_phoneNumberValidation:self.textField_contact.text] == false)
////    {
////        [[[UIAlertView alloc] initWithTitle:@"Sorry"
////                                    message:@"Invalid contact number"
////                                   delegate:nil
////                          cancelButtonTitle:@"Ok"
////                          otherButtonTitles:nil, nil]
////         show];
////    }
//    else if ([single method_checkOnlySpaces:self.textField_email.text] || [single method_NSStringIsValidEmail:self.textField_email.text] == false)
//    {
//        [[[UIAlertView alloc] initWithTitle:@"Sorry"
//                                    message:@"Invalid email address"
//                                   delegate:nil
//                          cancelButtonTitle:@"Ok"
//                          otherButtonTitles:nil, nil]
//         show];
//    }
//    else if ([single method_checkOnlySpaces:self.textField_address.text])
//    {
//        [[[UIAlertView alloc] initWithTitle:@"Sorry"
//                                    message:@"Enter your address 1"
//                                   delegate:nil
//                          cancelButtonTitle:@"Ok"
//                          otherButtonTitles:nil, nil]
//         show];
//    }
////    else if ([single method_checkOnlySpaces:self.textField_addressTwo.text])
////    {
////        [[[UIAlertView alloc] initWithTitle:@"Sorry"
////                                    message:@"Enter your address 2"
////                                   delegate:nil
////                          cancelButtonTitle:@"Ok"
////                          otherButtonTitles:nil, nil]
////         show];
////    }
//    else if ([single method_checkOnlySpaces:self.textField_city.text])
//    {
//        [[[UIAlertView alloc] initWithTitle:@"Sorry"
//                                    message:@"Enter your city"
//                                   delegate:nil
//                          cancelButtonTitle:@"Ok"
//                          otherButtonTitles:nil, nil]
//         show];
//    }
//    else if ([single method_checkOnlySpaces:self.textField_state.text])
//    {
//        [[[UIAlertView alloc] initWithTitle:@"Sorry"
//                                    message:@"Enter state"
//                                   delegate:nil
//                          cancelButtonTitle:@"Ok"
//                          otherButtonTitles:nil, nil]
//         show];
//    }
//    else if ([single method_checkOnlySpaces:self.textField_zipCode.text])
//    {
//        [[[UIAlertView alloc] initWithTitle:@"Sorry"
//                                    message:@"Enter zip code"
//                                   delegate:nil
//                          cancelButtonTitle:@"Ok"
//                          otherButtonTitles:nil, nil]
//         show];
//    }
//    else
//    {
        [self showSpinner];
        
        [self method_submitForm];
//        
//        [[[UIAlertView alloc] initWithTitle:nil
//                                    message:@"Wait for a while"
//                                   delegate:nil
//                          cancelButtonTitle:@"Ok"
//                          otherButtonTitles:nil, nil]
//         show];
   // }
}



#pragma mark - bar button
-(void)action_done
{
    self.textField_cardNumber.text = @"";
    self.textField_CVVTwo.text = @"";
    
    
    [self.textField_cardType resignFirstResponder];
    
    [self.textField_cardNumber becomeFirstResponder];
    
    self.textField_cardType.text = [array_cardType objectAtIndex:[self.pickerView selectedRowInComponent:0]];
    
    
    if ([self.textField_cardType.text isEqualToString:@"Visa"])
    {
        // typerang = 2;
        
        [self.imageView_CardType setImage:[UIImage imageNamed:@"visa"]];
        
        [self.textField_cardNumber setPlaceholder:@"0000 0000 0000 0000"];
        [self.textField_CVVTwo setPlaceholder:@"000"];
    }
    else if ([self.textField_cardType.text isEqualToString:@"Master Card"])
    {
        //  typerang = 2;
        
        [self.imageView_CardType setImage:[UIImage imageNamed:@"mastercard"]];
        
        [self.textField_cardNumber setPlaceholder:@"0000 0000 0000 0000"];
        [self.textField_CVVTwo setPlaceholder:@"000"];
    }
    else if ([self.textField_cardType.text isEqualToString:@"American Express"])
    {
        //  typerang = 3;
        
        [self.imageView_CardType setImage:[UIImage imageNamed:@"American_Express"]];
        
        [self.textField_cardNumber setPlaceholder:@"0000 000000 00000"];
        [self.textField_CVVTwo setPlaceholder:@"0000"];
    }
}




#pragma mark - picker delegate and data source
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isKindOfClass:[CDatePickerViewEx class]])
    {
        return 0;
    }
    return [array_cardType count]?[array_cardType count]:0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [array_cardType objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}



#pragma mark - alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - SelectedDate delegate
-(void)method_selectedDate:(NSString *)string_date
{
    self.textField_expiryDate.text = string_date;
}
@end