//
//  CrosshairsView.m
//  DMSDemo
//
//  Created by Sinclair, Eoin on 2/14/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import "CrosshairsView.h"

@implementation CrosshairsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    CGFloat offset = 6.0;
    CGFloat ratio = 0.3;
    CGRect middle = CGRectInset(rect, CGRectGetWidth(rect) * ratio, CGRectGetHeight(rect) * ratio);
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(c, [UIColor whiteColor].CGColor);
    CGContextSetShadowWithColor(c, CGSizeMake(2.0f, 2.0f), offset-1, [UIColor blackColor].CGColor);
    CGContextSetLineWidth(c, 1);
    CGContextSetLineCap(c, kCGLineCapRound);
    
    CGContextMoveToPoint    (c,  offset,                        CGRectGetMidY(rect)             );
    CGContextAddLineToPoint (c,  CGRectGetMinX(middle),         CGRectGetMidY(rect)             );
    
    CGContextMoveToPoint    (c,  CGRectGetMidX(rect),           offset                          );
    CGContextAddLineToPoint (c,  CGRectGetMidX(rect),           CGRectGetMinY(middle)           );

    
    CGContextMoveToPoint    (c,  CGRectGetMaxX(rect)-offset,    CGRectGetMidY(rect)             );
    CGContextAddLineToPoint (c,  CGRectGetMaxX(middle),         CGRectGetMidY(rect)             );
    
    CGContextMoveToPoint    (c,  CGRectGetMidX(rect),           CGRectGetMaxY(rect)-offset      );
    CGContextAddLineToPoint (c,  CGRectGetMidX(rect),           CGRectGetMaxY(middle)           );
    
    CGContextStrokePath(c);
}

@end
