#import "AugmentedRealityViewController.h"

#import "ZoomRotatePanImageView.h"
#import "MBProgressHUD.h"
#import "CustomSpinnerView.h"

#define optionViewWidth 150.0
#define optionViewHeight 50.0
#define K_PAUSE_INTERVAL 2.0

@interface AugmentedRealityViewController ()<ZoomAndPanDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
}
@property (retain)  MBProgressHUD *progressHUD;
@property (retain)  CustomSpinnerView *customSpinner;


@property(nonatomic, strong)NSTimer *myTimer;
@property(nonatomic, strong)UIImage *image_flipped;
@property(nonatomic, strong)UIImageView *imageView_currentImage;
@property (nonatomic) UIImagePickerController *imagePickerController;
@property (nonatomic) NSMutableArray *array_capturedImages;

@property (weak, nonatomic) IBOutlet UIImageView *imageView_backImage;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBaar_;

@property (strong, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet ZoomRotatePanImageView *viewSimple_;
@property (weak, nonatomic) IBOutlet UIView *view_options;



- (IBAction)action_photoLibrary:(id)sender;
- (IBAction)action_camera:(id)sender;

- (IBAction)action_cancel:(id)sender;
- (IBAction)action_capture:(id)sender;
- (IBAction)action_addNewImage:(id)sender;

- (IBAction)action_flip:(id)sender;


@end







@implementation AugmentedRealityViewController

#pragma mark - add remove loader
-(void)showSpinner
{
    self.progressHUD = [[MBProgressHUD alloc] initWithWindow:self.view.window];
    self.progressHUD.center = self.view.center;//self.view.center;
    self.customSpinner = [[CustomSpinnerView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self.progressHUD setCustomView:self.customSpinner];
    [self.progressHUD setMode:MBProgressHUDModeCustomView];
    [self.view.window addSubview:self.progressHUD];
    [self.progressHUD show:YES];
    
    //    NSTimer *t = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(removeSpinner) userInfo:0 repeats:NO];
    //    [[NSRunLoop mainRunLoop] addTimer:t forMode:NSRunLoopCommonModes];
}

-(void)removeSpinner
{
    self.progressHUD.removeFromSuperViewOnHide = YES;
    [self.progressHUD hide:YES];
    [self.customSpinner stopAnimation];
    self.progressHUD = nil;
    self.customSpinner = nil;
}



#pragma mark - add layer to object
-(void)method_addLayerToObject:(CALayer *)layer_
{
    [layer_ setBorderColor: [[UIColor whiteColor] CGColor]];
    [layer_ setBorderWidth: 0.5];
    [layer_ setShadowOffset:CGSizeMake(-3.0, 3.0)];
    [layer_ setShadowRadius:3.0];
    [layer_ setShadowOpacity:1.0];
    [layer_ setShadowColor:[[UIColor whiteColor] CGColor]];
}


#pragma mark - cycle
-(void)viewWillAppear:(BOOL)animated
{
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Augmented Reality Demo";
    
    DLog(@"%@", self.string_imageURL);
    
    [self method_addLayerToObject:self.view_options.layer];
    
        self.imageView_backImage.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapRecogniser = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(method_handleTap:)];
        [self.imageView_backImage addGestureRecognizer:tapRecogniser];
    
    
    
    self.array_capturedImages = [[NSMutableArray alloc] init];
    [self.array_capturedImages addObject:self.view_options];
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        // There is not a camera on this device, so don't show the camera button.
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"There is no camera in this device"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
    
    self.viewSimple_.delegate = self;
    
    
//    ZoomRotatePanImageView *imageView = [[ZoomRotatePanImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
//    imageView.delegate = self;
//    [imageView setImage:[UIImage imageNamed:self.string_imageURL]];
//    
//    NSLog(@"image name: %@", self.string_imageURL);
//    [imageView setBackgroundColor:[UIColor clearColor]];
//    imageView.center = self.view.center;
//    
//    
//    
//    [self.view addSubview:imageView];
//    
//    [self.array_capturedImages insertObject:imageView atIndex:0];
//    
//    imageView = nil;
    
    
    
    [self performSelector:@selector(showSpinner) withObject:nil afterDelay:0.1];
    
    
    // download the image asynchronously
    [self downloadImageWithURL:self.string_imageURL
               completionBlock:^(BOOL succeeded, UIImage *image) {
                   if (succeeded)
                   {
                       ZoomRotatePanImageView *imageView = [[ZoomRotatePanImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
                       imageView.delegate = self;
                       [imageView setImage:image];
                       [imageView setBackgroundColor:[UIColor clearColor]];
                       imageView.center = self.view.center;
                       
                       
                       
                       [self.view addSubview:imageView];
                       
                       [self.array_capturedImages insertObject:imageView atIndex:0];
                       
                       imageView = nil;
                   }
                   
                   [self performSelector:@selector(removeSpinner) withObject:nil afterDelay:0.1];
               }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - download image from URL
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}



#pragma mark - add new image
- (IBAction)action_addNewImage:(id)sender
{
    ZoomRotatePanImageView *imageView = [[ZoomRotatePanImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    imageView.delegate = self;
    [imageView setBackgroundColor:[UIColor lightGrayColor]];
    imageView.center = self.view.center;
    
    [self.view addSubview:imageView];
    
    [self.array_capturedImages insertObject:imageView atIndex:0];
    //    [self.array_capturedImages addObject:imageView];
    
    imageView = nil;
}


#pragma mark - action_flip
- (IBAction)action_flip:(id)sender
{
    self.imageView_currentImage.image = [UIImage imageWithCGImage:self.imageView_currentImage.image.CGImage
                                                            scale:self.imageView_currentImage.image.scale
                                                      orientation:(self.imageView_currentImage.image.imageOrientation == 0)?4:0];
    
    //      self.imageView_currentImage.transform = CGAffineTransformScale(self.imageView_currentImage.transform, -1.0, 1.0);
}


#pragma mark - action_photoLibrary
- (IBAction)action_photoLibrary:(id)sender
{
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

#pragma mark - action_camera
- (IBAction)action_camera:(id)sender
{
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
}

//http://stackoverflow.com/questions/18583461/easier-way-to-subview-a-uiimagepickercontroller-mines-not-working
//http://stackoverflow.com/questions/20590346/using-cameraoverlayview-with-uiimagepickercontroller
//https://developer.apple.com/library/ios/samplecode/PhotoPicker/Listings/PhotoPicker_APLViewController_m.html


#pragma mark - showImagePickerForSourceType
- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    if (self.imagePickerController)
    {
        self.imagePickerController = nil;
    }
    
    self.imagePickerController = [[UIImagePickerController alloc] init];
    self.imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.imagePickerController.sourceType = sourceType;
    self.imagePickerController.delegate = self;
    
    [self.imagePickerController setAllowsEditing:NO];
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        /*
         The user wants to use the camera interface. Set up our custom overlay view for the camera.
         */
        self.imagePickerController.showsCameraControls = NO;
        
        /*
         Load the overlay view from the OverlayView nib file. Self is the File's Owner for the nib file, so the overlayView outlet is set to the main view in the nib. Pass that view to the image picker controller to use as its overlay view, and set self's reference to the view to nil.
         */
        [[NSBundle mainBundle] loadNibNamed:@"OverlayView" owner:self options:nil];
        self.overlayView.frame = self.imagePickerController.cameraOverlayView.frame;
        
        
        
        [self.array_capturedImages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             [self.overlayView addSubview:obj];
         }];
        
        
        [self.view_options setHidden:YES];
        
        
        //        [self.overlayView addSubview:self.view_options];
        
        
        
        /*                  to stop zooming                 */
        
        //        self.imagePickerController.cameraOverlayView = self.overlayView;
        [self.imagePickerController.view addSubview:self.overlayView];
        
        
        /*                  to stop zooming                 */
        
        
        
        
        
        self.overlayView = nil;
        
        
        // Device's screen size (ignoring rotation intentionally):
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        // iOS is going to calculate a size which constrains the 4:3 aspect ratio
        // to the screen size. We're basically mimicking that here to determine
        // what size the system will likely display the image at on screen.
        // NOTE: screenSize.width may seem odd in this calculation - but, remember,
        // the devices only take 4:3 images when they are oriented *sideways*.
        float cameraAspectRatio = 4.0 / 3.0;
        float imageWidth = floorf(screenSize.width * cameraAspectRatio);
        float scale = ceilf((screenSize.height / imageWidth) * 10.0) / 10.0;
        
        self.imagePickerController.cameraViewTransform = CGAffineTransformMakeScale(scale, scale);
    }
    
    
    
    
    
    [self presentViewController:self.imagePickerController animated:YES completion:NULL];
}


#pragma mark - action_cancel
- (IBAction)action_cancel:(id)sender
{
    [self finishAndUpdate];
}

#pragma mark - action_capture
- (IBAction)action_capture:(id)sender
{
    [self.imagePickerController takePicture];
}


#pragma mark - finishAndUpdate
- (void)finishAndUpdate
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if ([self.array_capturedImages count] > 0)
    {
        [self.array_capturedImages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             [self.view addSubview:obj];
         }];
    }
    
    [self.view bringSubviewToFront:self.toolBaar_];
    self.imagePickerController = nil;
}



#pragma mark - UIImagePickerControllerDelegate
// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    [self.imageView_backImage setImage:image];
    
    [self finishAndUpdate];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark -
-(void)method_hideViews:(id)timer
{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^
     {
         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
         
         if (self.view_options.alpha == 0)
             self.view_options.alpha=1;
         else
         {
             self.view_options.alpha=0;
         }
     }
                     completion:^(BOOL b)
     {
     }
     ];
}


#pragma mark - ZoomAndPanDelegates
-(void)method_pan:(CGRect)rect obj:(UIImageView *)imageView_
{
    if (self.view_options.alpha == 0)
        self.view_options.alpha = 1;
    
    
    
    if (self.imageView_currentImage != imageView_)
    {
        self.imageView_currentImage = imageView_;
    }
    
    
    if (self.view_options.hidden)
        self.view_options.hidden = NO;
    
    [self.view bringSubviewToFront:self.view_options];
    
    
    
    if (rect.origin.x <= 0 && rect.origin.y <=  self.view_options.frame.size.height)
    {
        self.view_options.frame = CGRectMake(0,
                                             self.view.frame.size.height-self.view_options.frame.size.height,
                                             optionViewWidth,
                                             optionViewHeight);
        return;
    }
    
    
    if (rect.origin.x <= 0)                                             //for x-axis
    {
        self.view_options.frame = CGRectMake(0,
                                             rect.origin.y-self.view_options.frame.size.height,
                                             optionViewWidth,
                                             optionViewHeight);
        return;
    }
    
    
    if (rect.origin.y <=  self.view_options.frame.size.height)          //for y-axis
    {
        self.view_options.frame = CGRectMake(rect.origin.x+(rect.size.width-self.view_options.frame.size.width) / 2,
                                             0,
                                             optionViewWidth,
                                             optionViewHeight);
        return;
    }
    
    
    self.view_options.frame = CGRectMake(rect.origin.x+(rect.size.width-self.view_options.frame.size.width) / 2,
                                         rect.origin.y-self.view_options.frame.size.height,
                                         optionViewWidth,
                                         optionViewHeight);
}


-(void)method_resetFrames:(CGRect)rect obj:(UIImageView *)imageView_
{
    if (self.view_options.alpha == 0)
        self.view_options.alpha = 1;

    
    
    if (self.imageView_currentImage != imageView_)
    {
        self.imageView_currentImage = imageView_;
    }
    
    
    [self.view bringSubviewToFront:self.view_options];
    
    [UIView animateWithDuration:.25 animations:^{
        self.view_options.transform = CGAffineTransformIdentity;
        self.view_options.frame = CGRectMake(rect.origin.x+(rect.size.width-self.view_options.frame.size.width) / 2,
                                             rect.origin.y-self.view_options.frame.size.height,
                                             self.view_options.frame.size.width,
                                             self.view_options.frame.size.height);
    }];
}

-(void)method_gestureEnded
{
    if(self.myTimer)
        [self.myTimer invalidate];
    
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:K_PAUSE_INTERVAL
                                                    target:self
                                                  selector:@selector(method_hideViews:)
                                                  userInfo:nil
                                                   repeats:NO];
}




#pragma tap gesture
- (void)method_handleTap:(UITapGestureRecognizer*)recogniser
{
    NSLog(@"tag = %ld", (long)[recogniser.view tag]);
    NSLog(@"class = %@", [recogniser.view class]);
    
    if (![recogniser.view tag]==999)
        return;
    
    
    
    if(self.navigationController.navigationBar.isHidden)
    {
        [self.navigationController setNavigationBarHidden:NO
                                                 animated:YES];
        
        
        [self method_pan:self.imageView_currentImage.frame obj:self.imageView_currentImage];
        
        //        [self.tabBarController setTabBarHidden:NO
        //                                      animated:YES];
    }
    else
    {
        [self.navigationController setNavigationBarHidden:YES
                                                 animated:YES];
        
        [self method_pan:self.imageView_currentImage.frame obj:self.imageView_currentImage];
        
        //        [self.tabBarController setTabBarHidden:YES
        //                                      animated:YES];
    }
}


//- (IBAction)flipCamera:(id)sender {
//    if(cameraUI.cameraDevice == UIImagePickerControllerCameraDeviceFront)
//    {
//        cameraUI.cameraDevice = UIImagePickerControllerCameraDeviceRear;
//    }
//    else {
//        cameraUI.cameraDevice = UIImagePickerControllerCameraDeviceFront;
//    }
//    cameraUI.cameraViewTransform = CGAffineTransformScale(cameraUI.cameraViewTransform, -1,     1);
//}



@end