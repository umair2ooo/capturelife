#import "NewProductDetailViewController.h"

#import "MBProgressHUD.h"
#import "CustomSpinnerView.h"
#import "productDetailCollectionViewCell.h"
#import "ZoomRotatePanImageView.h"
#import "AugmentedRealityViewController.h"
#import "AsyncImageView.h"
#import "ProductsDataClass.h"
#import "LocationDataClass.h"
#import "OrderOnlineViewController.h"
#import "DirectionInStoreViewController.h"
#import "NewDirectionViewController.h"
#import "SubmitFormViewController.h"
#import "MapView.h"



#define k_data "data"
#define k_productName "productName"
#define k_price "price"
#define k_sale "sale"
#define k_inStock "inStock"
#define k_modelNo "modelNo"
#define k_skuNo "skuNo"
#define k_productDesc "productDesc"
#define k_detailImage "detailImage"
#define k_ARImage "ARImage"
#define k_prodRating "prodRating"
#define k_relatedProdId "relatedProdId"
#define k_storeName "storeName"
#define k_nearestStore "nearestStore"
#define k_locationInfo "locationInfo"







@interface NewProductDetailViewController ()<UIScrollViewDelegate>
{
    NSArray *array_gender;
    
    CGFloat ImageHeight;
    CGFloat ImageWidth;
    
    NSDictionary *dic_holdMyProfileData;
    
    NSMutableArray *array_products;
}

@property (retain)  MBProgressHUD *progressHUD;
@property (retain)  CustomSpinnerView *customSpinner;

@property(nonatomic, strong)ProductsDataClass *productsDataClass_obj;


@property (weak, nonatomic) IBOutlet UIButton *button_seeInYourHome;

@property (weak, nonatomic) IBOutlet UIButton *button_ratingOne;
@property (weak, nonatomic) IBOutlet UIButton *button_ratingTwo;
@property (weak, nonatomic) IBOutlet UIButton *button_ratingThree;
@property (weak, nonatomic) IBOutlet UIButton *button_ratingFour;
@property (weak, nonatomic) IBOutlet UIButton *button_ratingFive;

@property (weak, nonatomic) IBOutlet UILabel *lable_price;
@property (weak, nonatomic) IBOutlet UILabel *lable_sale;
@property (weak, nonatomic) IBOutlet UILabel *lable_inStock;

@property (weak, nonatomic) IBOutlet UILabel *label_productName;


@property (weak, nonatomic) IBOutlet UILabel *lable_modelNumber;
@property (weak, nonatomic) IBOutlet UILabel *label_SKUNumber;

@property (weak, nonatomic) IBOutlet UILabel *lable_productDesc;

@property (weak, nonatomic) IBOutlet UILabel *lable_Aisle;
@property (weak, nonatomic) IBOutlet UILabel *lable_Bay;




@property UIPickerView *picker_;


@property (strong, nonatomic) IBOutlet UIBarButtonItem *revealButtonItem;

@property(nonatomic, strong)NSMutableArray *pageViews;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView_subScroller;


@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UIView *view_scrollerContainer;
@property (weak, nonatomic) IBOutlet AsyncImageView *imageView_BackZoomer;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightBarButton_othersProfile;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftBarButton_othersProfile;

-(IBAction)action_exit:(UIStoryboardSegue *)segue;

@end

@implementation NewProductDetailViewController


#pragma mark - show and hide loader
-(void)showSpinner
{
    self.progressHUD = [[MBProgressHUD alloc] initWithWindow:self.view.window];
    self.progressHUD.labelText = @"Loading Image";
    self.progressHUD.center = self.view.center;
    self.customSpinner = [[CustomSpinnerView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self.progressHUD setCustomView:self.customSpinner];
    [self.progressHUD setMode:MBProgressHUDModeCustomView];
    [self.view.window addSubview:self.progressHUD];
    [self.progressHUD show:YES];
    
    //    NSTimer *t = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(removeSpinner) userInfo:0 repeats:NO];
    //    [[NSRunLoop mainRunLoop] addTimer:t forMode:NSRunLoopCommo                                                                                                                                                                                                                                                 nModes];
}

-(void)removeSpinner
{
    self.progressHUD.removeFromSuperViewOnHide = YES;
    [self.progressHUD hide:YES];
    [self.customSpinner stopAnimation];
    self.progressHUD = nil;
    self.customSpinner = nil;
}


#pragma mark - method_settingHorizontalImages
-(void)method_settingHorizontalImages:(NSArray *)images
{
    self.imageView_BackZoomer.image =  nil;
    
    // Set up the image we want to scroll & zoom and add it to the scroll view
    NSInteger pageCount = array_products.count;
    
    // Set up the page control
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = pageCount;
    
    // Set up the array to hold the views for each page
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i)
    {
        [self.pageViews addObject:[NSNull null]];
    }
    
    // Set up the content size of the scroll view
    CGSize pagesScrollViewSize = self.scrollView_subScroller.frame.size;
    self.scrollView_subScroller.contentSize = CGSizeMake(pagesScrollViewSize.width * array_products.count, 0);
    
    // Load the initial set of pages that are on screen
    [self loadVisiblePages];
}



#pragma mark - method_makeIDFromPayLoad
-(void)method_makeIDFromPayLoad
{
    NSArray *array_temp = [[self.dic_dataFromPrevControler valueForKey:k_payload] componentsSeparatedByString:@"."];
    
    DLog(@"self.dic_dataFromPrevControler: %@", self.dic_dataFromPrevControler);
    
    NSString *str_temp;
    
    if([[array_temp lastObject] rangeOfString:@"000"].location != NSNotFound)
    {
        DLog(@"id: %@", [[array_temp lastObject] stringByReplacingOccurrencesOfString:@"000" withString:@""]);
        
        str_temp = [[array_temp lastObject] stringByReplacingOccurrencesOfString:@"000" withString:@""];
        
        // send str_temp to server
        [self method_fetchData:str_temp];//////18244
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"No match found"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }


    array_temp = nil;
    str_temp = nil;
}



#pragma mark - method_setValuesOnXIB
-(void)method_setValuesOnXIB:(ProductsDataClass *)product
{
    self.lable_sale.text = product.p_sale;
    self.lable_productDesc.text = product.p_description;
    self.lable_price.text = product.p_price;
    self.lable_modelNumber.text = product.p_model;
    self.lable_inStock.text = product.p_inStock;
    self.label_SKUNumber.text = product.p_skuNumber;
    self.label_productName.text = product.p_name;



    switch (product.p_rating)
    {
        case 0:
            [self.button_ratingOne setSelected:NO];
            [self.button_ratingTwo setSelected:NO];
            [self.button_ratingThree setSelected:NO];
            [self.button_ratingFour setSelected:NO];
            [self.button_ratingFive setSelected:NO];
            break;
        case 1:
            [self.button_ratingOne setSelected:YES];
            [self.button_ratingTwo setSelected:NO];
            [self.button_ratingThree setSelected:NO];
            [self.button_ratingFour setSelected:NO];
            [self.button_ratingFive setSelected:NO];
            break;
        case 2:
            [self.button_ratingOne setSelected:YES];
            [self.button_ratingTwo setSelected:YES];
            [self.button_ratingThree setSelected:NO];
            [self.button_ratingFour setSelected:NO];
            [self.button_ratingFive setSelected:NO];
            break;
        case 3:
            [self.button_ratingOne setSelected:YES];
            [self.button_ratingTwo setSelected:YES];
            [self.button_ratingThree setSelected:YES];
            [self.button_ratingFour setSelected:NO];
            [self.button_ratingFive setSelected:NO];
            break;
        case 4:
            [self.button_ratingOne setSelected:YES];
            [self.button_ratingTwo setSelected:YES];
            [self.button_ratingThree setSelected:YES];
            [self.button_ratingFour setSelected:YES];
            [self.button_ratingFive setSelected:NO];
            break;
        case 5:
            [self.button_ratingOne setSelected:YES];
            [self.button_ratingTwo setSelected:YES];
            [self.button_ratingThree setSelected:YES];
            [self.button_ratingFour setSelected:YES];
            [self.button_ratingFive setSelected:YES];
            break;
            
        default:
            break;
    }
    
    
    NSArray *array_temp = [NSArray arrayWithArray:product.array_location];
    
    [array_temp enumerateObjectsUsingBlock:^(LocationDataClass *obj, NSUInteger idx, BOOL *stop)
    {
        if (obj.location_nearestStore == YES)
        {
            self.lable_Aisle.text = obj.location_Asle;
            self.lable_Bay.text = obj.location_Bay;
            *stop = YES;
        }
    }];

    
    [self method_settingHorizontalImages:array_products];
}



#pragma mark - method_fetchData
- (void)method_fetchData:(NSString *)id_
{
    [self performSelector:@selector(showSpinner) withObject:nil afterDelay:0.1];
    
    
    DLog(@"%@", [NSString stringWithFormat:@"http://webservice.royalcyber.net/CaptureLife/CaptureLifeWebservice/?method=get&format=json&id=%@", id_]);
    
    
    
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://119.63.131.236:88/CaptureLife/CaptureLifeWebservice/?method=get&format=json&id=%@", id_]];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://webservice.royalcyber.net/CaptureLife/CaptureLifeWebservice/?method=get&format=json&id=%@", id_]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         [self performSelector:@selector(removeSpinner) withObject:nil afterDelay:0.1];


         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:0
                                                                        error:NULL];

             DLog(@"responseData: %@", responseData);

             if ([responseData isKindOfClass:[NSNull class]] || responseData == nil)
             {
                 [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                             message:@"There is something is wrong from server side, please try later"
                                            delegate:nil
                                   cancelButtonTitle:@"Ok"
                                   otherButtonTitles:nil, nil]
                  show];
             }
             else
             {
                 if (array_products)
                 {
                     [array_products removeAllObjects];
                     array_products = nil;
                 }

                 array_products = [[NSMutableArray alloc] init];


                 [(NSArray *)[responseData valueForKey:@k_data] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
                  {
                      ProductsDataClass *obj_ = [[ProductsDataClass alloc] init];

                      [obj_ setP_id:[obj valueForKey:k_id]];
                      [obj_ setP_name:[obj valueForKey:@k_productName]];
                      [obj_ setP_rating:[[obj valueForKey:@k_prodRating] intValue]];
                      [obj_ setP_price:[obj valueForKey:@k_price]];
                      [obj_ setP_sale:[obj valueForKey:@k_sale]];
                      [obj_ setP_inStock:[obj valueForKey:@k_inStock]];
                      [obj_ setP_model:[obj valueForKey:@k_modelNo]];
                      [obj_ setP_skuNumber:[obj valueForKey:@k_skuNo]];
                      [obj_ setP_description:[obj valueForKey:@k_productDesc]];
                      
//                      [[obj valueForKey:@k_detailImage] stringByReplacingOccurrencesOfString:@"119.63.131.236:88" withString:@"192.168.16.48:88"]
//                      [[obj valueForKey:@k_ARImage] stringByReplacingOccurrencesOfString:@"119.63.131.236:88" withString:@"192.168.16.48:88"]
                      
                      [obj_ setP_imageURL:[NSURL URLWithString:[obj valueForKey:@k_detailImage]]];
                      [obj_ setP_imageURL_AR:[NSURL URLWithString:[obj valueForKey:@k_ARImage]]];
                      
//                      [obj_ setP_imageURL:[NSURL URLWithString:[[obj valueForKey:@k_detailImage] stringByReplacingOccurrencesOfString:@"119.63.131.236:88" withString:@"192.168.16.48:88"]]];
//                      [obj_ setP_imageURL_AR:[NSURL URLWithString:[[obj valueForKey:@k_ARImage] stringByReplacingOccurrencesOfString:@"119.63.131.236:88" withString:@"192.168.16.48:88"]]];
                      
                      
                      [obj_ setArray_location:(NSMutableArray *)[obj valueForKey:@k_locationInfo]];
                      
                      [array_products addObject:obj_];
                      
                      //                 DLog(@"%@", obj_.p_id);
                      //                 DLog(@"%@", obj_.p_name);
                      //                 DLog(@"%d", obj_.p_rating);
                      //                 DLog(@"%@", obj_.p_price);
                      //                 DLog(@"%@", obj_.p_sale);
                      //                 DLog(@"%@", obj_.p_inStock);
                      //                 DLog(@"%@", obj_.p_model);
                      //                 DLog(@"%@", obj_.p_skuNumber);
                      //                 DLog(@"%@", obj_.p_imageURL);
                      //                 DLog(@"%@", obj_.p_imageURL_AR);
                      //                 DLog(@"%@", obj_.array_location);
                      
                      obj_ = nil;
                  }];
                 
                 
                 self.productsDataClass_obj = (ProductsDataClass *)[array_products objectAtIndex:0];
                 [self method_setValuesOnXIB:(ProductsDataClass *)[array_products objectAtIndex:0]];
             }
         }
         else
         {
             [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                         message:@"There is something is wrong from server side, please try later"
                                        delegate:nil
                               cancelButtonTitle:@"Ok"
                               otherButtonTitles:nil, nil]
              show];
         }
     }];
}


 
#pragma mark - controller cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
//    self.title = @"Product detail";
    
    CGRect bounds = self.view.bounds;
    self.scroller.frame = bounds;
    
    self.scroller.alwaysBounceVertical = YES;
    
    ImageWidth = self.imageView_BackZoomer.frame.size.width;
    ImageHeight = self.imageView_BackZoomer.frame.size.height;
    
    
    self.scroller.backgroundColor = [UIColor clearColor];
    
    self.navigationController.navigationBar.translucent = NO;
    if (self.string_uId)        // it means coming from others profile
    {
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = self.leftBarButton_othersProfile;
        
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = self.rightBarButton_othersProfile;
    }
    else
    {
        //        [self.revealButtonItem setTarget: self.revealViewController];
        //        [self.revealButtonItem setAction: @selector( revealToggle: )];
        //        [self.navigationController.navigationBar addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    
    [self method_makeIDFromPayLoad];
    
    
//    NSMutableArray *array_temp = [[NSMutableArray alloc] init];
//    
//    for (unsigned i = 0; i<[array_products count]; i++)
//    {
//        [array_temp addObject:[[array_products objectAtIndex:i] valueForKey:k_id]];
//    }
//    
//    [self method_settingHorizontalImages:array_temp];
//    
//    array_temp = nil;
}


-(void)viewDidLayoutSubviews
{
    [self.scroller setContentSize:CGSizeMake(0, self.view_scrollerContainer.frame.size.height)];
}


//- (void)setTranslatesAutoresizingMaskIntoConstraints:(BOOL)flag
//{
//    
//}


- (void)viewDidUnload
{
    self.scrollView_subScroller = nil;
    self.pageControl = nil;
    array_products = nil;
    self.pageViews = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - method_hideSubScrollerToMaximizeTheVisibleImage
-(void)method_hideSubScrollerToMaximizeTheVisibleImage:(BOOL)isGoingToHide
{
    if (isGoingToHide == true)
    {
        [self.scrollView_subScroller setHidden:YES];
        
        // First find the visible image
        CGFloat pageWidth = self.scrollView_subScroller.frame.size.width;
        NSInteger page = (NSInteger)floor((self.scrollView_subScroller.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
        
        // then pass the visible image to ImageView which is on the back
        
        if ([array_products count])
        {
            //cancel loading previous image for cell
//            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:self.imageView_BackZoomer];
            
            
            //set placeholder image or cell won't update when image is loaded
            self.imageView_BackZoomer.image = [UIImage imageNamed:@"Placeholder.png"];
            
            //load the image
            
            self.productsDataClass_obj = [array_products objectAtIndex:page];
  
            [self.imageView_BackZoomer loadImageFromURL:[self.productsDataClass_obj.p_imageURL absoluteString]];
//            self.imageView_BackZoomer.imageURL = self.productsDataClass_obj.p_imageURL;
            
            
            
            
//            [self.imageView_BackZoomer setImage:[UIImage imageNamed:[array_products objectAtIndex:page]]];
            
            //            [self.imageView_BackZoomer setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",
            //                                                                             [[array_images objectAtIndex:page] valueForKey:@"image"]]]
            //                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
            //             {
            //                 // image downloaded.. do your work
            //             }];
        }
    }
    else
    {
        [self.scrollView_subScroller setHidden:NO];
        self.imageView_BackZoomer.image =  nil;
    }
}



#pragma mark - scrollView delegates
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView tag] == 1)
    {
        CGFloat yOffset   = self.scroller.contentOffset.y;
        
        DLog(@"yOffset: %f", yOffset);
        
        if (yOffset < 0)
        {
            [self method_hideSubScrollerToMaximizeTheVisibleImage:YES];
            
            
            
            CGFloat factor = ((ABS(yOffset)+ImageHeight)*ImageWidth)/ImageHeight;
            CGRect f = CGRectMake(-(factor-ImageWidth)/2, 0, factor, ImageHeight+ABS(yOffset));
            self.imageView_BackZoomer.frame = f;
            
            
        }
        else
        {
            [self method_hideSubScrollerToMaximizeTheVisibleImage:NO];
            
            
            CGRect f = self.imageView_BackZoomer.frame;
            f.origin.y = -yOffset;
            self.imageView_BackZoomer.frame = f;
        }
    }
    else if([scrollView tag] == 2)
    {
        // Load the pages which are now on screen
        [self loadVisiblePages];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (self.scrollView_subScroller != scrollView)
        return;
    
    
    [self.button_seeInYourHome setEnabled:NO];
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (self.scrollView_subScroller != scrollView)
        return;
    
    self.productsDataClass_obj = (ProductsDataClass *)[array_products objectAtIndex:(long)self.pageControl.currentPage];
    [self method_setValuesOnXIB:[array_products objectAtIndex:self.pageControl.currentPage]];
    
//    [array_products enumerateObjectsUsingBlock:^(ProductsDataClass *obj, NSUInteger idx, BOOL *stop)
//     {
//         DLog(@"Alhamdulillah===============");
//         
//         NSArray *array = [NSArray arrayWithArray:obj.array_location];
//          
//         [array enumerateObjectsUsingBlock:^(LocationDataClass *obj, NSUInteger idx, BOOL *stop)
//          {
//              DLog(@"location_storeImageURL: %@", obj.location_storeImageURL);
//              DLog(@"location_storeName: %@", obj.location_storeName);
//              
//          }];
//     }];

    
    
    [self.button_seeInYourHome setEnabled:YES];
    DLog(@"currentPage: %ld", (long)self.pageControl.currentPage);
    
    
//    [self.dic_dataFromPrevControler setObject:[[array_products objectAtIndex:self.pageControl.currentPage] valueForKey:k_imageURL] forKey:k_imageURL];
}




#pragma mark - method_setLayer_radius
-(CALayer *)method_setLayer_radius:(float)radius brdrColor:(UIColor *)brdrColor brdrWidth:(float)brdrWidth layer:(CALayer *)layer
{
    [layer setCornerRadius:radius];
    layer.borderColor = brdrColor.CGColor;
    layer.borderWidth = brdrWidth;
    [layer setMasksToBounds:YES];
    
    layer.shadowOffset = CGSizeMake(0, 3);
    layer.shadowRadius = 4.0;
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOpacity = 0.8;
    return layer;
    
    
    //another way
    
    
    //    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.collectionView_.bounds];
    //    self.collectionView_.layer.masksToBounds = NO;
    //    self.collectionView_.layer.shadowColor = [UIColor blackColor].CGColor;
    //    self.collectionView_.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    //    self.collectionView_.layer.shadowOpacity = 0.5f;
    //    self.collectionView_.layer.shadowPath = shadowPath.CGPath;
}


//#pragma mark - prepareForSegue
//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([segue.identifier isEqualToString:@"segue_VC_editProfile"])
//    {
////        UINavigationController *nav = (UINavigationController *)segue.destinationViewController;
//        
//        //        VC_editProfile *obj = (VC_editProfile *)nav.viewControllers[0];
//        //        obj.dic_dataFromPrevController = dic_holdMyProfileData;
//    }
//}

//#pragma mark - UIPickerView DataSource
//// returns the number of 'columns' to display.
//- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
//{
//    return 1;
//}
//
//// returns the # of rows in each component..
//- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
//{
//    return [array_gender count];
//}
//
//#pragma mark - UIPickerView Delegate
//- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
//{
//    return 30.0;
//}
//
//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    return [array_gender objectAtIndex:row];
//}
//
////If the user chooses from the pickerview, it calls this function;
//- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
//{
//    //Let's print in the console what the user had chosen;
//    //    self.TF_gender.text = [array_gender objectAtIndex:row];
//    NSLog(@"Chosen item: %@", [array_gender objectAtIndex:row]);
//    
//}


#pragma mark - action_exit
-(IBAction)action_exit: (UIStoryboardSegue *)segue
{
    DLog(@"exit with cancel");
}



#pragma mark -
#pragma mark - horizontal scroller
#pragma mark -
- (void)loadVisiblePages
{
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView_subScroller.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView_subScroller.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    // Update the page control
    self.pageControl.currentPage = page;
    
//    DLog(@"_________currentPage: %ld", (long)self.pageControl.currentPage);
    
    // Work out which pages we want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++)
    {
        [self purgePage:i];
    }
    for (NSInteger i=firstPage; i<=lastPage; i++)
    {
        [self loadPage:i];
    }
    for (NSInteger i=lastPage+1; i<array_products.count; i++)
    {
        [self purgePage:i];
    }
}

- (void)loadPage:(NSInteger)page
{
    if (page < 0 || page >= array_products.count)
    {
        // If it's outside the range of what we have to display, then do nothing
        return;
    }
    
    // Load an individual page, first seeing if we've already loaded it
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null])
    {
        CGRect frame = self.scrollView_subScroller.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        
        
        AsyncImageView *imgView_temp = [[AsyncImageView alloc] initWithFrame:CGRectMake(frame.origin.x, 0, 320, 320)];
        __weak AsyncImageView *imgView = imgView_temp;
        
        if ([array_products count])
        {
            //cancel loading previous image for cell
//            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imgView];
        

            //set placeholder image or cell won't update when image is loaded
            imgView.image = [UIImage imageNamed:@"Placeholder.png"];
        
            //load the image
            
            self.productsDataClass_obj = [array_products objectAtIndex:page];
            
//            imgView.imageURL = self.productsDataClass_obj.p_imageURL;
            [imgView loadImageFromURL:[self.productsDataClass_obj.p_imageURL absoluteString]];
            
//            DLog(@"detailImage: %@", [[array_products objectAtIndex:page] valueForKey:@"detailImage"]);
            
            
            //            [imgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",
            //                                                           [[array_images objectAtIndex:page] valueForKey:@"image"]]]
            //                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
            //             {
            //                 //            self.imageView_BackZoomer.image = image;
            //                 [imgView removeActivityIndicator];
            //             }
            //         usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        
        
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.frame = CGRectMake(frame.origin.x, 0, 320, 320);
        [imgView setBackgroundColor:[UIColor clearColor]];
        [self.scrollView_subScroller addSubview:imgView];
        [self.pageViews replaceObjectAtIndex:page withObject:imgView];
    }
}

- (void)purgePage:(NSInteger)page
{
    if (page < 0 || page >= array_products.count)
    {
        // If it's outside the range of what we have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null])
    {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - prepare for segue
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender NS_AVAILABLE_IOS(6_0)
{
    if ([identifier isEqualToString:@"segue_mapDirection"])
    {
        if (![CLLocationManager locationServicesEnabled])
        {
            [[[UIAlertView alloc] initWithTitle:@"This app does not have access to Location service"
                                        message:@"You can enable access in Settings->Privacy->Location->Location Services"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
            
            return NO;
        }
    }
    
    
    return YES;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_insideStore"])
    {
        DirectionInStoreViewController *obj = (DirectionInStoreViewController *)segue.destinationViewController;

        self.productsDataClass_obj = [array_products objectAtIndex:self.pageControl.currentPage];
        obj.array_stores = self.productsDataClass_obj.array_location;
        return;
    }
    
    
    if ([segue.identifier isEqualToString:@"segue_mapDirection"])
    {
        NewDirectionViewController *obj = (NewDirectionViewController *)segue.destinationViewController;
        
        self.productsDataClass_obj = [array_products objectAtIndex:self.pageControl.currentPage];
        obj.array_stores = self.productsDataClass_obj.array_location;
        return;
    }
    
    
    if ([segue.identifier isEqualToString:@"segue_submitForm"])
    {
        SubmitFormViewController *obj = (SubmitFormViewController *)segue.destinationViewController;

        self.productsDataClass_obj = [array_products objectAtIndex:self.pageControl.currentPage];
        obj.productsDataClass_obj = self.productsDataClass_obj;
        return;
    }
    
    
    
    
    
    
    if ([segue.identifier isEqualToString:@"segue_direction"])
    {
//        DirectionViewController *obj = (DirectionViewController *)segue.destinationViewController;
//        obj.string_url = [self.dic_dataFromPrevControler valueForKey:k_content];
        return;
    }
    
    if ([segue.identifier isEqualToString:@"segue_orderOnline"])
    {
        OrderOnlineViewController *obj = (OrderOnlineViewController *)segue.destinationViewController;
        obj.string_url = [self.dic_dataFromPrevControler valueForKey:k_content];
        return;
    }
    
    
    if ([segue.identifier isEqualToString:@"segue_augmentedReality"])
    {
        AugmentedRealityViewController *obj = (AugmentedRealityViewController *)segue.destinationViewController;
        
//        obj.string_imageURL = [NSString stringWithFormat:@"%@", [self.dic_dataFromPrevControler valueForKey:k_imageURL]];
        
        self.productsDataClass_obj = [array_products objectAtIndex:self.pageControl.currentPage];
        obj.string_imageURL = self.productsDataClass_obj.p_imageURL_AR;
        return;
    }
}
@end