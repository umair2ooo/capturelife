//
//  AudioVisualizer.m
//  DMSDemo
//
//  Created by Sinclair, Eoin on 2/11/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import "AudioVisualizer.h"
#import <AVFoundation/AVFoundation.h>

#define BUFFCOUNT               250
#define kDownsampleResolution   8
#define kAudioBoost             300.0

@interface AudioVisualizer () {
    NSLock  *audioDataLock;
}
@property (nonatomic) float* audioData;
@property (nonatomic) int audioDataCapacity;
@property (nonatomic) int dataCount;

@property (weak) CADisplayLink *link;

@end

@implementation AudioVisualizer

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
        [self initialize];
    return self;
}

-(id)init {
    if (self = [super init])
        [self initialize];
    return self;
}

-(void)initialize {
    self.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1];

    audioDataLock = [[NSLock alloc] init];
    self.audioData = (float*) calloc(BUFFCOUNT, sizeof(float));
    self.audioDataCapacity = BUFFCOUNT;
    self.dataCount = 0;
}

-(void)setVisualizerDataWithData:(float *)audioSamples count:(int)count{
    if (!audioSamples || !count) {
        self.dataCount = 0;
        return;
    }

    // incoming is on audio thread, take care to protect data from async drawing on main UI thread
    if([audioDataLock tryLock]) {
        int i, j;
        for(i=0, j=0; i<self.audioDataCapacity && j<count; i++, j+=kDownsampleResolution) {
            self.audioData[i] = audioSamples[j];
        }
        self.dataCount = i;
        [audioDataLock unlock];
    }
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    // Do as much of the heavy graphics context setup as possible before grabbing the data lock
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetShouldAntialias(c, false);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 0.5, 1.0 };
    NSArray *colors = @[(__bridge id)[UIColor colorWithWhite:0.15 alpha:1].CGColor,
                        (__bridge id)[UIColor colorWithWhite:0.8 alpha:1].CGColor,
                        (__bridge id)[UIColor colorWithWhite:0.15 alpha:1].CGColor
                        ];
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    CGContextSaveGState(c);
    CGContextAddRect(c, rect);
    CGContextClip(c);
    CGContextDrawLinearGradient(c, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(c);
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
    
    CGFloat zero = CGRectGetMidY(rect);
    UIBezierPath *path = [UIBezierPath bezierPath];
    UIColor *white = [UIColor whiteColor];
    [white setStroke];

    // if data is locked for incoming audio thread updates,
    // just skip drawing the waveform and pick it up on next cycle
    //
    if([audioDataLock tryLock]) {
        if (self.audioData && self.dataCount) {
            CGFloat ratio = CGRectGetWidth(rect) / self.dataCount;
            ratio+=0.1;
            
            [path moveToPoint:CGPointMake(0, zero)];
            
            for (int i=0; i<self.dataCount; i++) {
                int x = i * ratio;
                if (x > CGRectGetMaxX(rect)) {
                    break;
                }
                int y = zero + self.audioData[i] * kAudioBoost;
                [path addLineToPoint:CGPointMake(x, y)];
            }
            
            // release the lock before doing the expensive stroke
            [audioDataLock unlock];
            [path stroke];
        } else {
            [audioDataLock unlock];
        }
    }
}

-(void)dealloc {
    [self.link invalidate];
    self.link = nil;

    [audioDataLock lock];
    if(self.audioData) {
        free(self.audioData);
        self.audioData = nil;
    }
    self.audioDataCapacity = 0;
    self.dataCount = 0;
    [audioDataLock unlock];
    audioDataLock = nil;
}

@end
