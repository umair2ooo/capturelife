/****************************************************************************************************************
*
* The technology detailed in this software is the subject of various pending and issued patents,
* both internationally and in the United States, including one or more of the following patents:
*
* 5,636,292 C1; 5,710,834; 5,832,119 C1; 6,286,036; 6,311,214; 6,353,672; 6,381,341; 6,400,827;
* 6,516,079; 6,580,808; 6,614,914; 6,647,128; 6,681,029; 6,700,990; 6,704,869; 6,813,366;
* 6,879,701; 6,988,202; 7,003,132; 7,013,021; 7,054,465; 7,068,811; 7,068,812; 7,072,487;
* 7,116,781; 7,158,654; 7,280,672; 7,349,552; 7,369,678; 7,461,136; 7,564,992; 7,567,686;
* 7,590,259; 7,657,057; 7,672,477; 7,720,249; 7,751,588; and EP 1137251 B1; EP 0824821 B1;
* and JP-3949679, all owned by Digimarc Corporation.
*
* Use of such technology requires a license from Digimarc Corporation, USA.  Receipt of this software
* conveys no license under the foregoing patents, nor under any of Digimarc’s other patent, trademark,
* or copyright rights.
*
* This software comprises CONFIDENTIAL INFORMATION, including TRADE SECRETS, of Digimarc Corporation,
* USA, and is protected by a license agreement and/or non-disclosure agreement with Digimarc.  It is
* important that this software be used, copied and/or disclosed only in accordance with such
* agreements.
*
* © Copyright, Digimarc Corporation, USA.  All Rights Reserved.
*
****************************************************************************************************************/

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>
#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>

/** @class DMResolver
* See the @ref DMResolverGroup "DMResolver API" for complete documentation of this class.
* 
*/
/** @defgroup DMResolverGroup DMResolver API
* DMResolver resolves DMPayload to DMPayoff translation, via web service queries to Digimarc Resolver service.
*
* This class implements methods for managing the resolver, resolving a payload, reporting actions taken by the user,
* and handling the resolution result (payoff).
*
* @resolverThreadsNote
*/

/** Error domain string associated with any NSError objects associated with DMResolver logic.
*
* See also underlying network, NSURL, NSURLConnection, NSHTTPConnection errors which will be passed
* through DMResolver without translation to DMResolver error codes.
* @ingroup DMResolverGroup
*/
extern NSString* const DigimarcResolverErrorDomain;

/** DMPayload types that are not supported by DMResolverSDK or the DDIM resolve api.
* @ingrouop DMResolverGroup
*/
#define kDMResolverErrorUnknownPayloadType 401

/** Resolve request was canceled
* @ingroup DMResolverGroup
*/
#define kDMResolverErrorRequestCanceled 402

/** Attempted to "resolve" a DMPayload containing a QRCode payload value that is not a URL.
*
* The Digimarc DDIM resolver does not actually support resolving any QRCodes,
* but to provide a consistent interface with other watermark and barcode types,
* the DMResolver API will attempt to simulate a DMStandardPayoff result when passed a QRCode containing a URL.
*
* This error message results when a QRCode is passed in that cannot be translated into a displayable standard payoff.
* @ingroup DMResolverGroup
*/
#define kDMResolverErrorQRCodeDoesNotContainURL 403

@class DMPayload;
@class DMResolveResult;

/**
* The different types of payoffs.
* @ingroup DMPayoffGroup
*/
typedef enum DMPayoffType : NSUInteger
{
	DMPayoffTypeStandard = 0,		///< A simple redirect URL, along with title, optional description, and optional thumbnail image.
    DMPayoffTypeInactive = 2			///< This payload exists in DDIM, but its associated service is either expired or not currently activated.
} DMPayoffType;

/**
* @typedef ResolveCompletionBlock
* Application-defined block to be called on async completion of resolving a payload to a payoff, or in case of an error.
*
* @code typedef void (^ResolveCompletionBlock)(DMResolveResult* result, NSError* error); @endcode
*
* @param result The DMResolveResult provided by DDIM in response to a previous resolve query.
* @param error Error code if resolve query failed.
* @ingroup resolvePayload
*/
typedef void (^ResolveCompletionBlock)(DMResolveResult* result, NSError* error);

/**
* @typedef IsAvailableCompletionBlock
* Application-defined block to be called on async completion of testing availability of the resolver web service.
*
* @code typedef void (^IsAvailableCompletionBlock)(BOOL available, NSError* error); @endcode
*
* @param available   YES if network available, service URL was reachable, and service responded to query.  Otherwise NO.
* @param error Error code if query failed.
* @ingroup isAvailable
*/
typedef void (^IsAvailableCompletionBlock)(BOOL available, NSError* error);

/**
* @typedef ReportActionCompletionBlock
* Application-defined block to be called on async completion of reporting the given action or in case of an error.
*
* @code typedef void (^ReportActionCompletionBlock)(NSString* actionToken, NSError* error); @endcode
*
* @param actionToken An actionToken string from a previously resolved DMPayoff.   DDIM dispenses unique action tokens for each successful resolve query.
* @param error Error code if report action failed.
* @ingroup reportAction
*/
typedef void (^ReportActionCompletionBlock)(NSString* actionToken, NSError* error);

/**
* @typedef DownloadImageCompletionBlock
* Application-defined block to be called on async completion of a requested image download, or in case of an error.
*
* @code typedef void (^DownloadImageCompletionBlock)(UIImage* image, NSError* error); @endcode
*
* @param image UIImage in memory after download from URL.  May be nil if error.
* @param error Error code if download failed.  Otherwise nil.
* @ingroup  downloadImage
*/
typedef void (^DownloadImageCompletionBlock)(UIImage *image, NSError* error);

/*-------------------------------------------------------------------------------------*/
#pragma mark - DMResolver Interface
@interface DMResolver : NSObject { }

/**
* Generic utility method.
*
* Serializes the given dictionary contents into the returned JSON string.
*
* @param dataToSerialize Pointer to any (non empty) NSDictionary or NSMutableDictionary.
* @result a logically equivalent graph of JSON objects in a single string.
* @ingroup DMResolverGroup
*/
+ (NSString*) serializeDictionary:(NSDictionary*)dataToSerialize;

/**
* Returns the singleton Resolver instance.
*
* Applications should use this method rather than constructing their own instance with alloc, init.
* @ingroup DMResolverGroup
*/
+ (DMResolver *)sharedDMResolver;

/** @defgroup  isAvailable resolverAvailable query API
*
* Async DMResolver method to determine resolver availability.
*
* @resolverThreadsNote
*
* @ingroup DMResolverGroup
*/
/**
* Determines whether the Digimarc Resolver web service is available.
*
* This asynchronous method performs a
* simple network operation to confirm network connectivity and an expected response from the service.
* It's use is optional and its intended for debugging or testing the network prior to resolving a Payload.
*
* The web service URL is taken from the DMResolver serviceURL property.
*
* @param completionBlock This block will be called when query completes or on error.
* @ingroup isAvailable
*/
- (void) resolverAvailable:(IsAvailableCompletionBlock)completionBlock;

/** @defgroup  resolvePayload resolve API
*
* Async DMResolver method to resolve (translate) a payload to a payoff.
*
* @resolverThreadsNote
*
* @ingroup DMResolverGroup
*/
/**
* Resolves the given payload to a payoff.
*
* This is an asynchronous method that can take several seconds to complete, depending on network conditions.
*
* @param payload A payload object previously derived form an image watermark, audio watermark, or barcode read.
* @param completionBlock This block will be called on resolve query completed or error.
* @ingroup resolvePayload
*/
-(void) resolve:(DMPayload*)payload
completionBlock:(ResolveCompletionBlock)completionBlock;

/**
* 
*
* @deprecated (DMSDK 1.4.0) Location tracking has been removed from DMSDK.  This DMResolver resolve:withLocation:completionBlock: method will be removed in a future distribution of DMSDK.
* In the meantime, any location passed in to this API will be ignored, with remaining params relayed to resolve:completionBlock: method.
*
* @ingroup resolvePayload
*/
-(void) __attribute__((deprecated)) resolve:(DMPayload*)payload
   withLocation:(CLLocation *)location
completionBlock:(ResolveCompletionBlock)completionBlock;

/**
* Cancels resolution of the given payload.
*
* @param payload  Payload to be cancelled. Will be compared with all payloads currently in the DMNetworkManager queue
* as currently in progress.
* @result  Returns YES if cancellation succeeded, NO otherwise.
* @ingroup resolvePayload
*/
- (BOOL) cancelResolve:(DMPayload*)payload;

/**
* Cancels all outstanding resolutions.
*
* @result Returns YES if cancellation succeeded, NO otherwise.
* @ingroup resolvePayload
*/
- (BOOL) cancelAllResolves;

/** @defgroup  reportAction reportAction user-action reporting API
*
* Async DMResolver method to report user's viewing of payoff content.
*
* @resolverThreadsNote
*
* @ingroup DMResolverGroup
*/
/**
* Report the user's viewing of payoff content.
*
* Calling this method records metrics about payoff content each time the user actually views the content.
* Applications are required to use this method for proper usage reporting.
*
* In some applications the watermark payload detection, resolve, and payoff will all go toward display
* of a banner ad or other clickable content.  The reportAction reporting should take place if and when
* the user clicks through to view the actual payoff content.
*
* @param actionToken  An actionToken from a previously resolved DMPayoff.
* @param completionBlock  This block will be called on completion of the reportAction posting or error.
* @ingroup reportAction
*/
- (void) reportAction:(NSString*)actionToken
      completionBlock:(ReportActionCompletionBlock)completionBlock;

/** @defgroup  downloadImage downloadImage API
*
* Async DMResolver method to download a payoff thumbnail image.
*
* @resolverThreadsNote
*
* @ingroup DMResolverGroup
*/
/** 
* Start a request to asynchronously download an image from a URL.
*
* @param imageURL A thumbnailImageURL from a previously resolved DMPayoff.
* @param completionBlock  This block will be called on download completed or error.
* @ingroup  downloadImage
*/
- (void) downloadImage:(NSURL*)imageURL
       completionBlock:(DownloadImageCompletionBlock)completionBlock;

/**
* 
*
* @deprecated (DMSDK 1.3.0) All DMSDK components are now tracked with the single DMSManager version property.
* This DMResolver version property will be removed in a future distribution of DMSDK.
* In the meantime, this property will simply return the DMSManager version.
*
* @ingroup DMResolverGroup
*/
@property (readonly, nonatomic) __attribute__((deprecated)) NSString * version;

/**
* Login credentials for the resolver web service.
*
* The userName and password enable access to the resolver web service. These must be set before
* attempting to resolve any payloads. They are supplied by Digimarc and are unique per application.
* @ingroup DMResolverGroup
*/
@property (nonatomic, retain) NSString * userName;
/**
* Login credentials for the resolver web service.
*
* The userName and password enable access to the resolver web service. These must be set before
* attempting to resolve any payloads. They are supplied by Digimarc and are unique per application.
* @ingroup DMResolverGroup
*/
@property (nonatomic, retain) NSString * password;

/**
* The URL of the resolver web service.
*
* This is supplied by Digimarc and should not be changed unless instructed to by Digimarc.
*
* Default: Production Resolver Web Service
* @ingroup DMResolverGroup
*/
@property (nonatomic, retain) NSURL * serviceURL;

@end

/*-------------------------------------------------------------------------------------*/
#pragma mark - DMPayoff Interface
/** @class DMPayoff
* See the @ref DMPayoffGroup "DMPayoff API" for complete documentation of this class.
* 
*/
/**
* @defgroup DMPayoffGroup DMPayoff API
*
* DMPayoff is an abstract base class for user visible metadata associated with a watermark.
*
* The concrete payoff class types returned by DMResolver include:
* - DMStandardPayoff
* - DMInactivePayoff
*/
@interface DMPayoff : NSObject

/**
* A brief descriptive title for this payoff.  It is intended for display to the user in a payoff list or summary in the UI.
* @ingroup DMPayoffGroup
*/
@property (nonatomic, retain) NSString * title;

/**
* This field is optional.  If present, it provides additional information to be shown in a payoff list or summary in the UI.
* @ingroup DMPayoffGroup
*/
@property (nonatomic, retain) NSString * subtitle;

/**
* This field is optional.  If present, a scaled version of this image should appear with the title and subtitle in the UI.
* @ingroup DMPayoffGroup
*/
@property (nonatomic, retain) NSURL * thumbnailImageURL;

/**
* This field is optional, and is rarely used.
*
* Indicates which of nine area of interests within the image is the focal point.   The focal
* point is the area defined by the content owner in the DDIM, as the region of the original 
* image that will contain the watermark.
*
*      -------------------------------
*      | 0.0,0.0 | 0.5,0.0 | 1.0,0.0 |
*      -------------------------------
*      | 0.0,0.5 | 0.5,0.5 | 1.0,0.5 |
*      -------------------------------
*      | 0.0,1.0 | 0.5,1.0 | 1.0,1.0 |
*      -------------------------------
* @ingroup DMPayoffGroup
*/
@property (nonatomic) CGPoint thumbnailImageFocalPoint;

@end


/*-------------------------------------------------------------------------------------*/
#pragma mark - DMStandardPayoff Interface
/** @class DMStandardPayoff
*
* A subclass of DMPayoff for an active payoff.
*
* A DMStandardPayoff defines a single content URL, along with title, subtitle, and thumbnail image metadata for
* display to the user in a payoffs list or history UI. All payoff metadata is defined by a content owner or campaign manager via the Digimarc Online Service Portal.
* Within the DDIM, the payoff is associated with an active service.
*
* See the @ref DMStandardPayoffGroup "DMStandardPayoff Module" for complete documentation of this class.
* @ingroup DMPayoffGroup
*/
@interface DMStandardPayoff : DMPayoff

@property (readonly, retain) NSString * content;		///< The single content URL associated with this payoff within the DDIM service.  @ingroup DMPayoffGroup
@property (readonly, retain) NSString * actionType;		///< Typically @"Redirect".  @ingroup DMPayoffGroup
@property (readonly, retain) NSString * actionToken;	///< A unique token for the resolve query that returned this payoff.  This token must be reported back to DDIM if/when the user actually views this content.  See also DMResolver reportAction API.  @ingroup DMPayoffGroup

@end

/*-------------------------------------------------------------------------------------*/
#pragma mark - DMInactivePayoff Interface
/** @class DMInactivePayoff
 *
 * A subclass of DMPayoff for a payoff that has not been activated or has expired on the Digimarc Online Services Portal.
 *
 * A DMInactivePayoff inherits basic properties from DMPayoff, but in practice does not contain any useful
 * payoff data and should be handled by either ignoring this payload/payoff, or by displaying an error message to the user.
 *
 * See the @ref DMPayoffGroup "DMPayoff API" for complete documentation of this class.
 * @ingroup DMPayoffGroup
 */
@interface DMInactivePayoff : DMPayoff
@end

/*-------------------------------------------------------------------------------------*/
#pragma mark - DMResolveResult Interface
/** @class DMResolveResult
* See the @ref DMResolverGroup "DMResolver API" for complete documentation of this class.
* 
*/
/** @defgroup DMResolveResultGroup DMResolveResult API
*
* The DMResolveResult is returned to the resolve completion block.  The resolve result wraps the original DMPayload
* that initiated the resolve, the DMPayoff returned from the Digimarc Resolver, along with some summary information
* and resolve elapsed time diagnostics.
*
* @ingroup DMResolverGroup
*/
@interface  DMResolveResult : NSObject

/**
* DMPayoffType is an enumerated type indicating the type of payoff
*
*      - standard (DMPayoffTypeStandard)
*      - inactive (DMPayoffTypeInactive)
*
* @ingroup DMResolveResultGroup
*/
@property DMPayoffType payoffType;

/**
* The payoff object.   The payoff object contains the user visible metadata associated with a detected watermark.  This is 
* defined within the Digimarc Discover Identity Manager (DDIM) system, and associated with the watermark via the Digimarc Resolver.
*
* See also DMPayoff.
*
* @ingroup DMResolveResultGroup
*/
@property (retain) DMPayoff * payoff;

/**
* The payload which triggered this resolve query and associated payoff.
*
* In the presence of multiple payloads, the application
* can use this to ensure that it is handling the proper payoff for each payload.
* @ingroup DMResolveResultGroup
*/
@property (readonly, retain) DMPayload * payload;

/**
* Time taken for the resolve call.
*
* For diagnostic purposes only.
*/
@property int resolveTimeMs;

@end

