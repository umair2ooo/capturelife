//
//  CppRuntime.cpp
//  Digimarc Mobile SDK (DMS)
//
//  Created by localTstewart on 7/17/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

// This dummy file is used to trick Xcode into correctly providing the C++ std libraries runtime
// required by some of the lower level C++ readers.   No actual code is required, just at least
// one *.cpp file included in the linker files list.
//