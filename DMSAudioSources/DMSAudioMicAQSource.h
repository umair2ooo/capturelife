//
//  DMSAudioMicAQSource.h
//  Digimarc Mobile SDK (DMS)
//
//  Created by localTstewart on 7/23/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DMSDelegateProtocol.h"

/** @defgroup DMSAudioMicAQSourceGroup Example Class DMSAudioMicAQSource
* @brief This class illustrates an implementation of the @ref DMSAudioSourceProtocol "DMSAudioSourceProtocol" &mdash; see @ref DMSAudioSourceProtocol "DMSAudioSourceProtocol" for details on the protocol and the @ref DMSAudioMicAQSource "DMSAudioMicAQSource Implementation" to review the implementation itself.
* @ingroup DMSMediaSourceGroup
*
*/
@interface DMSAudioMicAQSource : NSObject <DMSAudioSourceProtocol>

/** audio mic source specific methods
 */

@property NSString* name;
@property (weak) DMSManager* dmsManager;
@property BOOL isRunning;
@property BOOL enableStallDetection;

@property (weak) id<DMSAudioVisualizerProtocol> delegate;

/**
 * @brief Call this optional method if you want DMSAudioMicAQSource to take over the application AVAudioSession. 
 *
 * This includes setting the AVAudioSession category PlayAndRecord, mixWithOthers, and audio interruption listener.
 * On beginning or ending an interruption, the audio source is stopped/restarted automatically (though this sequence
 * happens only if user does not proceed with the interruption e.g. taking a phone call).  In this case the application
 * entering background should close out the current DMS session and open a new session on returning
 * to foreground mode.
 *
 * If you are planning to implement your own application AVAudioSession management, do not call this method &mdash; your app will maintain complete control.
 * @ingroup DMSAudioMicAQSourceGroup
 */
- (BOOL) setupAVAudioSession;

/** required DMSAudioSourceProtocol methods
 */
- (BOOL) attachToDms:(DMSManager*)dms;
- (BOOL) detachFromDms:(DMSManager*)dms;

- (BOOL) start;
- (void) stop;

- (BOOL) dmsShouldPerformSynchronousReadsOnIncomingThread;

- (int) sampleRate;
- (int) numChannels;
- (int) bitsPerSample;

@end
