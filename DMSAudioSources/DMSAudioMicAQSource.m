//
//  DMSAudioMicAQSource.m
//  Digimarc Mobile SDK (DMS)
//
//  Created by localTstewart on 7/23/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#import "AudioToolbox/AudioToolbox.h"
#import "DMSAudioMicAQSource.h"
#import "DMSManager.h"

#define kNumberAudioDataBuffers	10

@interface DMSAudioMicAQSource() {
    int _sampleRate;
    int _numChannels;
    int _bitsPerSample;
    int _bytesPerSample;
    int _bytesPerFrame;

    AudioStreamBasicDescription mAudioFormat;
    AudioQueueRef mQueueObject;
    AVAudioSession* audioSession;
    
    NSTimer* _stallDetectionTimer;
}

@property BOOL wasRunningWhenInterrupted;
@property BOOL manageAVSessionAndInterruptionListener;

@property int timeCounter;
@property int prevTimeCounter;
@property int buffersCounter;
@property int prevBuffersCounter;

- (void) setAVAudioSessionActive:(BOOL)bActive;
- (void) mediaServicesResetNotificationHandler:(NSNotification*)notification;
- (void) interruptionNotificationHandler:(NSNotification*)notification;
- (void) beginInterruption;
- (void) endInterruption;

- (void) clearStallDetectionCounters;
- (void) startStallDetection;
- (void) stopStallDetection;
- (void) processStallTimerTick:(NSTimer*)t;

- (void) processIncomingAudioBuffer:(AudioQueueBufferRef)buffer forQueue:(AudioQueueRef)queue;
- (void) runVisualizer:(AudioQueueBufferRef)buffer;

@end

static void DMSAudioMicAQ_recordingCallback(void *inUserData,
                                     AudioQueueRef inAudioQueue,
                                     AudioQueueBufferRef inBuffer,
                                     const AudioTimeStamp *inStartTime,
                                     UInt32 inNumPackets,
                                     const AudioStreamPacketDescription *inPacketDesc
                                     )
{
    DMSAudioMicAQSource* audioMic = (__bridge DMSAudioMicAQSource*)inUserData;
    if (inNumPackets > 0) {
        [audioMic processIncomingAudioBuffer:inBuffer forQueue:inAudioQueue];
        [audioMic runVisualizer:inBuffer];
    }
}

@implementation DMSAudioMicAQSource

- (id) init {
    self = [super init];
    if(self) {
        
        self.name = @"DMSAudioMicAQSource";
        
        self.dmsManager = nil;
        self.isRunning = NO;
        self.wasRunningWhenInterrupted = NO;

        mQueueObject = 0;
        memset((void*)&mAudioFormat, 0, sizeof(mAudioFormat));

        _sampleRate = 16000;
        _numChannels = 1;
        _bitsPerSample = 32;
        _bytesPerSample = 4;
        _bytesPerFrame = 4;
        
        [self clearStallDetectionCounters];
        _stallDetectionTimer = nil;
        
        audioSession = [AVAudioSession sharedInstance];

    }
    return self;
}

- (void) dealloc {
    [self stop];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.dmsManager = nil;
}

- (BOOL) start {
    if(! self.isRunning) {

        // MAINTENANCE NOTE:    REQUIIRED AUDIO FORMAT
        //
        // Platform audio capture Channels, BitsPerSample, etc
        // must match settings expected by AWMDecoders (PCM Mono, 16bit SInt16 LE, or 32bit float LE)
        //
        
        mAudioFormat.mSampleRate		= _sampleRate;
        mAudioFormat.mFormatID			= kAudioFormatLinearPCM;
        mAudioFormat.mFormatFlags		= kAudioFormatFlagIsFloat | kAudioFormatFlagIsPacked;
        mAudioFormat.mChannelsPerFrame	= 1;
        mAudioFormat.mFramesPerPacket	= 1;
        mAudioFormat.mBitsPerChannel	= 32;
        mAudioFormat.mBytesPerPacket	= 4;
        mAudioFormat.mBytesPerFrame		= 4;
        
        // Create the queueObject
        //
        OSStatus	status = AudioQueueNewInput(&mAudioFormat,
                                                DMSAudioMicAQ_recordingCallback,
                                                (__bridge void *)(self),
                                                NULL,
                                                NULL,
                                                0,
                                                &mQueueObject);
        
        if (mQueueObject && status == noErr) {
            // get the actual recording format back from the audio queue's A/D converter --
            // this will sometimes vary from the requested format
            //
            UInt32	sizeOfRecordingFormatASBDStruct = sizeof(mAudioFormat);
            status = AudioQueueGetProperty(mQueueObject,
                                           kAudioQueueProperty_StreamDescription,
                                           &mAudioFormat,
                                           &sizeOfRecordingFormatASBDStruct);
            if(status == noErr) {
                // allocate and enqueue incoming audio buffers to the queueObject's pool
                //
                UInt32 bufferByteSize = 4096;
                for (unsigned long bufferIndex = 0;
                     bufferIndex < kNumberAudioDataBuffers && status == noErr;
                     ++bufferIndex)
                {
                    AudioQueueBufferRef buffer = 0;
                    status = AudioQueueAllocateBuffer(mQueueObject, bufferByteSize, &buffer);
                    if (status == noErr) {
                        AudioQueueEnqueueBuffer(mQueueObject, buffer, 0, NULL);
                    }
                }
                
                [self setAVAudioSessionActive:YES];
                
                status = AudioQueueStart(mQueueObject, NULL);
            }
        }
        
        if (status != noErr) {
            [self setAVAudioSessionActive:NO];
            if(mQueueObject) {
                AudioQueueStop(mQueueObject, TRUE);
                AudioQueueDispose(mQueueObject, TRUE);
                mQueueObject = nil;
            }
            [self.dmsManager postWarningFrom:self.name message:@"initialization failed"];
        } else {
            NSLog(@"DMSAudioMicAQSource up and running");
            
            if(self.enableStallDetection) {
                [self startStallDetection];
            }
            self.isRunning = YES;
        }
    }
    return YES;
}

- (void) stop {
    [self stopStallDetection];
    
    if(mQueueObject) {
		OSStatus	status = AudioQueueStop(mQueueObject, TRUE);
		if(status != noErr) {
            NSString* msg = [NSString stringWithFormat:@"AudioQueueStop reported error:%d, ignored", (int)status];
            [self.dmsManager postWarningFrom:self.name message:msg];
        }
        
        status = AudioQueueDispose(mQueueObject, TRUE);
        if(status != noErr) {
            NSString* msg = [NSString stringWithFormat:@"AudioQueueDispose reported error:%d, ignored", (int)status];
            [self.dmsManager postWarningFrom:self.name message:msg];
        }
	}
    [self setAVAudioSessionActive:NO];
    self.isRunning = NO;
}


- (void) processIncomingAudioBuffer:(AudioQueueBufferRef)buffer forQueue:(AudioQueueRef)queue {
    
    @autoreleasepool {
        
        self.buffersCounter++;
        
        if(self.dmsManager) {
            NSData* data = [NSData dataWithBytes:buffer->mAudioData length:buffer->mAudioDataByteSize];
            int numSamples = (int)data.length / _bytesPerFrame;
            [self.dmsManager incomingAudioBuffer:data numSamples:numSamples];
        }
        
        if (self.isRunning) {
            (void) AudioQueueEnqueueBuffer(queue, buffer, 0, NULL);
        }
    }
}

-(void) runVisualizer:(AudioQueueBufferRef)buffer {
    if(self.delegate && [self.delegate respondsToSelector:@selector(audioVisualizerDataAvailableWithData:count:)]) {
        int sampleCount = buffer->mAudioDataBytesCapacity / sizeof (float);
        float *p = (float*)buffer->mAudioData;
        [self.delegate audioVisualizerDataAvailableWithData:p count:sampleCount];
    }
}

#pragma mark - stall detection

- (void) clearStallDetectionCounters {
    self.buffersCounter = 0;
    self.prevBuffersCounter = 0;
    self.timeCounter = 0;
    self.prevTimeCounter = 0;
}

- (void) startStallDetection {
    [self stopStallDetection];
    
    _stallDetectionTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(processStallTimerTick:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop ] addTimer:_stallDetectionTimer forMode:NSRunLoopCommonModes];
}

- (void) stopStallDetection {
    if(_stallDetectionTimer) {
        [_stallDetectionTimer invalidate];
        _stallDetectionTimer = nil;
    }
    [self clearStallDetectionCounters];
}

- (void) processStallTimerTick:(NSTimer*)t {
    self.timeCounter++;
    
    if((self.timeCounter - self.prevTimeCounter) > 0) {
        if(! ((self.buffersCounter - self.prevBuffersCounter) > 0) ) {
            [self stop];
            [self start];
            [self.dmsManager postWarningFrom:self.name message:@"Audio input stalled, restarted"];
            return;
        }
    }
    self.prevTimeCounter = self.timeCounter;
    self.prevBuffersCounter = self.buffersCounter;
}


#pragma mark - optional AVSession handling

- (BOOL) setupAVAudioSession {
    self.manageAVSessionAndInterruptionListener = YES;
    
    NSError *err = nil;
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers error:&err];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mediaServicesResetNotificationHandler:)
                                                 name:AVAudioSessionMediaServicesWereResetNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(interruptionNotificationHandler:)
                                                 name:AVAudioSessionInterruptionNotification
                                               object:nil];

    return (err == nil);
}

- (void) setAVAudioSessionActive:(BOOL)bActive {
    if(self.manageAVSessionAndInterruptionListener) {
        [audioSession setActive:bActive error:nil];
    }
}

- (void) mediaServicesResetNotificationHandler:(NSNotification*)notification {
    [self stop];
    [self start];
    [self.dmsManager postWarningFrom:self.name message:@"AVAudioSessionMediaServices were reset, audio source restarted"];
}

- (void) interruptionNotificationHandler:(NSNotification*)notification {
    NSDictionary* notificationInfo = notification.userInfo;
    if(notificationInfo) {
        NSNumber* interruptionTypeCode = [notificationInfo objectForKey:AVAudioSessionInterruptionTypeKey];
        NSInteger interruptionType = [interruptionTypeCode integerValue];
        switch(interruptionType) {
            case AVAudioSessionInterruptionTypeBegan:
                NSLog(@"AVAudioSessionInterruptionHandler: Begin Interruption");
                [self beginInterruption];
                break;
            case AVAudioSessionInterruptionTypeEnded:
                NSLog(@"AVAudioSessionInterruptionHandler: Ended Interruption");
                [self endInterruption];
                break;
            default:
                NSLog(@"AVAudioSessionInterruptionHandler: Unexpected interruption type:%d, ignored", (int)interruptionType);
                break;
        }
    }
}

- (void) beginInterruption {
    self.wasRunningWhenInterrupted = self.isRunning;
    [self stop];
}

- (void) endInterruption {
    if(self.wasRunningWhenInterrupted) {
        [self start];
        self.wasRunningWhenInterrupted = NO;
    }
}


#pragma mark - required DMSAudioSourceProtocol methods

- (BOOL) attachToDms:(DMSManager*)dms {
    self.dmsManager = dms;

    return YES;
}

- (BOOL) detachFromDms:(DMSManager*)dms {
    [self stop];
    self.dmsManager = nil;
    
    return YES;
}

- (BOOL) dmsShouldPerformSynchronousReadsOnIncomingThread {
    return YES;
}

- (int) sampleRate {
    return _sampleRate;
}
- (int) numChannels {
    return _numChannels;
}
- (int) bitsPerSample {
    return _bitsPerSample;
}


@end

