//
//  DMSImageFileSource.h
//  Digimarc Mobile SDK (DMS)
//
//  Created by localTstewart on 7/22/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DMSDelegateProtocol.h"

@interface DMSImageFileSource : NSObject <DMSImageSourceProtocol>

// image file specific methods

@property (weak) DMSManager* dmsManager;
@property NSString* name;
@property NSMutableArray* filesList;
@property BOOL isRunning;

- (void) start;
- (void) imageProcessingThread;
- (void) stop;

// required DMSImageSourceProtocol methods

- (BOOL) dmsShouldPerformSynchronousReadsOnIncomingThread;

- (int) currentFrameRate;
- (int) requestFrameRate:(int)newValue;

- (BOOL) supportsAutoFocus;
- (BOOL) currentAutoFocusOn;
- (BOOL) requestAutoFocusOn:(BOOL)newValue;

- (BOOL) supportsTorch;
- (BOOL) currentTorchOn;
- (BOOL) requestTorchOn:(BOOL)newValue;


@end
