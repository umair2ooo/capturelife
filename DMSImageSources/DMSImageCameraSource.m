/***************************************************************************************************
 
 The technology detailed in this software is the subject of various pending and issued patents,
 both internationally and in the United States, including one or more of the following patents:
 
 5,636,292 C1; 5,710,834; 5,832,119 C1; 6,286,036; 6,311,214; 6,353,672; 6,381,341; 6,400,827;
 6,516,079; 6,580,808; 6,614,914; 6,647,128; 6,681,029; 6,700,990; 6,704,869; 6,813,366;
 6,879,701; 6,988,202; 7,003,132; 7,013,021; 7,054,465; 7,068,811; 7,068,812; 7,072,487;
 7,116,781; 7,158,654; 7,280,672; 7,349,552; 7,369,678; 7,461,136; 7,564,992; 7,567,686;
 7,590,259; 7,657,057; 7,672,477; 7,720,249; 7,751,588; and EP 1137251 B1; EP 0824821 B1;
 and JP-3949679, all owned by Digimarc Corporation.
 
 Use of such technology requires a license from Digimarc Corporation, USA.  Receipt of this software
 conveys no license under the foregoing patents, nor under any of Digimarc’s other patent, trademark,
 or copyright rights.
 
 This software comprises CONFIDENTIAL INFORMATION, including TRADE SECRETS, of Digimarc Corporation,
 USA, and is protected by a license agreement and/or non-disclosure agreement with Digimarc.  It is
 important that this software be used, copied and/or disclosed only in accordance with such
 agreements.
 
 © Copyright, Digimarc Corporation, USA.  All Rights Reserved.
 
***************************************************************************************************/
#import <CoreMotion/CoreMotion.h>
#import "DMSImageCameraSource.h"
#import "DMSManager.h"

@interface DMSImageCameraSource() {
    dispatch_queue_t dispatch;
    dispatch_source_t _focusTimer;
}

- (void)createCaptureSesssion;

@end;

@implementation DMSImageCameraSource

- (void) dealloc
{
    [self stop];
}

- (id) init
{
    if (self = [super init])
	{
		self.name = @"DMSImageCameraSource";
        
        // YUV420 is suitable for all iOS devices except the iPhone3G (use BGRA for that model)
        self.captureVideoFormat = kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange;
		
        // DMSManager parent connection will be captured during DMS Attach.
        self.dmsManager = nil;
    }
	
	return self;
}

// Configure the AVCaptureSession object for Video capture
- (void)createCaptureSesssion
{
    // Set up the session, capture device, and capture input and output connections
    //
    NSError*	error = nil;

    self.captureSession = [[AVCaptureSession alloc] init];
    self.captureSession.usesApplicationAudioSession = NO;
    
    self.device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];

    // Camera Focus optimization as used by Digimarc Discover application.
    // Whenever the camera subject image changes substantially, kick off an AutoFocusRangeRestrictionNear, followed by an explicit AutoFocus event.
    // After 5 seconds with no subject change, clear the AutoFocusRangeRestrictionNear, and another explicit AutoFocus event.
    //
    [self.device lockForConfiguration:nil];
    self.device.subjectAreaChangeMonitoringEnabled = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:self.device];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(captureDeviceSubjectAreaDidChangeNotification:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:self.device];
    [self.device unlockForConfiguration];
    
    AVCaptureInput*	captureInput = [[AVCaptureDeviceInput alloc] initWithDevice:self.device error:&error];
    if(!captureInput) {
        [self.dmsManager postWarningFrom:self.name message:@"AVCaptureDeviceInput failed"];
    } else {
        [self.captureSession addInput:captureInput];
    }
    
    AVCaptureVideoDataOutput*	videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    if(! videoOutput) {
        [self.dmsManager postWarningFrom:self.name message:@"AVCaptureVideoDataOutput failed"];
    } else {
        videoOutput.videoSettings = @{(id)kCVPixelBufferPixelFormatTypeKey: @(self.captureVideoFormat)};
        videoOutput.alwaysDiscardsLateVideoFrames = YES;
        [videoOutput setSampleBufferDelegate:self queue:dispatch];

        [self.captureSession addOutput:videoOutput];
    }

    // Use the DMSManager helper method to initialize the Capture Session and Device with recommended settings for image watermark detection.
    // These optimized settings vary by iOS device model, camera type, and iOS version.
    //
    [DMSManager initializeCaptureSession:self.captureSession andDevice:self.device];

}

- (void) captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBufferRef fromConnection:(AVCaptureConnection *)connection
{
    // callback from AVCaptureOutput
    //
	CVImageBufferRef imageBufferRef = CMSampleBufferGetImageBuffer(sampleBufferRef);
	if (imageBufferRef)
	{
        [self.dmsManager incomingImageBuffer:imageBufferRef];
	}
}

#pragma mark -
#pragma mark Notifications


- (void) addNotificationListeners
{
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(sessionRuntimeErrorNotification:)
                                         	   name:AVCaptureSessionRuntimeErrorNotification
                                             object:nil];
}             

- (void) removeNotificationListeners
{
	[[NSNotificationCenter defaultCenter] removeObserver:self 
    								                name:AVCaptureSessionRuntimeErrorNotification 
                                                  object:nil];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:self.device];
} 

// handle errors with the capture session by reporting to DMSManager as warnings (optimistic)

- (void) sessionRuntimeErrorNotification:(NSNotification *)notification
{	
	NSError *error = nil;
    
	if (notification != nil)
    {
    	error = (notification.userInfo)[AVCaptureSessionErrorKey];
        NSString* msg = [NSString stringWithFormat:@"AVCaptureSessionRuntimeErrorNotification:  %@: %@",
                         [error localizedDescription],
                         [error localizedFailureReason]];
        
        // downgrade error to a warning within DMSManager reporting, it may not be fatal
        //
        [self.dmsManager postWarningFrom:self.name message:msg];

        // Apple's recommended fix for the black camera view issue.  Recreate the AVCaptureSession.
        //
        if (error.code == AVErrorMediaServicesWereReset)
        {
            // you need to tear down all the AVCapture objects and start over
            [self.dmsManager postWarningFrom:self.name message:@"Apple was right.  Need to tear down AVCapture objects and restart."];
            
            self.captureSession = nil;
            [self createCaptureSesssion];
        }
    }
}

-(void)captureDeviceSubjectAreaDidChangeNotification:(NSNotification *)notification {

    // First, test to see if we can range restrict auto focus;
    // older devices and tablets may not support this.
    //
    if(self.device.autoFocusRangeRestrictionSupported)
    {
        [self.device lockForConfiguration:nil];
        self.device.autoFocusRangeRestriction = AVCaptureAutoFocusRangeRestrictionNear;
        self.device.focusMode = AVCaptureFocusModeAutoFocus;
        [self.device unlockForConfiguration];
        
        //if there is an existing timer to unlock the range restriction, cancel it
        //
        if(_focusTimer) {
            dispatch_source_cancel(_focusTimer);
        }
        
        // create a new timer and handler event.
        // timer will fire 5 seconds from now to set the focus range back to none, and kick off another AutoFoucs event.
        //
        _focusTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch);
        dispatch_source_set_timer(_focusTimer, dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), DISPATCH_TIME_FOREVER, 0);
        dispatch_source_set_event_handler(_focusTimer, ^{
            [self.device lockForConfiguration:nil];
            self.device.autoFocusRangeRestriction = AVCaptureAutoFocusRangeRestrictionNone;
            self.device.focusMode = AVCaptureFocusModeContinuousAutoFocus;
            [self.device unlockForConfiguration];
            
            //don't fire the timer again
            dispatch_source_cancel(_focusTimer);
        });
        
        //start the new timer
        dispatch_resume(_focusTimer);
    }
}

#pragma mark - DMSImageSourceProtocol required methods

- (BOOL) attachToDms:(DMSManager*)dms {
    self.dmsManager = dms;
    return YES;
}

- (void) detachFromDms:(DMSManager*)dms {
    [self stop];
    self.dmsManager = nil;
}

- (BOOL) start
{
	[self addNotificationListeners];
    
	// Bail if capture is already underway
	if (self.device != nil && self.captureSession != nil && dispatch != NULL)
		return YES;
	
	// Create our capture queue
	dispatch = dispatch_queue_create("com.digimarc.capturequeue", 0);
    if(dispatch == NULL) {
        [self.dmsManager postWarningFrom:self.name message:@"Failed to create queue"];
    }
	
	// Create the AVCapture Session
    [self createCaptureSesssion];
    [self.captureSession startRunning];
    
    return YES;
}

- (void) stop
{
	[self.captureSession stopRunning];
	self.captureSession = nil;

//	if (dispatch) {
//		dispatch_release(dispatch);
//  }
	dispatch = NULL;
	
	self.device = nil;
    [self removeNotificationListeners];
}


- (BOOL) dmsShouldPerformSynchronousReadsOnIncomingThread {
    return NO;
}

// TODO: implement camera frame rate min/max API
- (int) currentFrameRate {
    return 24;
}
- (int) requestFrameRate:(int)newValue {
    return 24;
}

// TODO: implement camera autoFocus API
- (BOOL) supportsAutoFocus {
    return NO;
}
- (BOOL) currentAutoFocusOn {
    return NO;
}
- (BOOL) requestAutoFocusOn:(BOOL)newValue {
    return NO;
}

// TODO: implement camera torch API
- (BOOL) supportsTorch {
    return NO;
}
- (BOOL) currentTorchOn {
    return NO;
}
- (BOOL) requestTorchOn:(BOOL)newValue {
    return NO;
}

@end
