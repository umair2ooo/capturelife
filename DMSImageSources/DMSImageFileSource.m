//
//  DMSImageFileSource.m
//  Digimarc Mobile SDK (DMS)
//
//  Created by localTstewart on 7/22/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#import "DMSImageFileSource.h"
#import "DMSManager.h"
#import "DMReader.h"

@implementation DMSImageFileSource

- (id) init {
    self = [super init];
    if(self) {
    	self.name = @"DMSImageFileSource";
        self.filesList = [[NSMutableArray alloc] init];
        self.isRunning = NO;
    }
    return self;
}

#pragma mark - DMSImageFileSource specific methods

- (void) dealloc {
    self.isRunning = NO;
    self.filesList = nil;
}

- (void) imageProcessingThread {
    while(self.isRunning && self.filesList.count > 0) {
        NSString* filename = [self.filesList objectAtIndex:0];
        [self.filesList removeObjectAtIndex:0];
        
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        UIImage *testImage = [UIImage imageWithContentsOfFile:[bundle pathForResource:filename ofType:nil]];
        if(! testImage) {
            NSString* msg = [NSString stringWithFormat:@"failed to load image: %@", filename];
            [self.dmsManager postWarningFrom:self.name message:msg];
        } else {
            [self.dmsManager imageMediaChanged:filename];
            [self.dmsManager incomingImage:testImage];
        }
    }
}

#pragma mark - required DMSImageSourceProtocol methods

- (BOOL) attachToDms:(DMSManager*)dms {
    self.dmsManager = dms;
    return YES;
}

- (void) detachFromDms:(DMSManager*)dms {
    self.dmsManager = nil;
}

- (void) start {
    if(! self.isRunning) {
        self.isRunning = YES;
        [self performSelectorInBackground:@selector(imageProcessingThread) withObject:nil];
    }
}

- (void) stop {
    self.isRunning = NO;
}

- (BOOL) dmsShouldPerformSynchronousReadsOnIncomingThread {
    // We want DMSManager to block our image file reader source thread from continuing until it is completely through with the image,
    // then we can release our in-memory image and move on.
    //
    return YES;
}

- (int) currentFrameRate {
    return 1;
}

- (int) requestFrameRate:(int)newValue {
    return 1;
}

- (BOOL) supportsAutoFocus {
    return NO;
}

- (BOOL) currentAutoFocusOn {
    return NO;
}

- (BOOL) requestAutoFocusOn:(BOOL)newValue {
    return NO;
}

- (BOOL) supportsTorch {
    return NO;
}

- (BOOL) currentTorchOn {
    return NO;
}

- (BOOL) requestTorchOn:(BOOL)newValue {
    return NO;
}


@end
