#import "MacysOnlineViewController.h"

#import "FBNetworkReachability.h"

@interface MacysOnlineViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView_;

@end

@implementation MacysOnlineViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if ([FBNetworkReachability sharedInstance].reachable)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //Here your non-main thread.
            NSLog (@"Hi, I'm new thread");
            [self.webView_ loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www1.macys.com/"]]];
            dispatch_async(dispatch_get_main_queue(), ^{
                //Here you returns to main thread.
                NSLog (@"Hi, I'm main thread");
            });
        });
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:k_networkError
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated
{
    if([self.webView_ isLoading])
    {
        [self.webView_ stopLoading];
    }
}

@end